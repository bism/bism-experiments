# Inline Monitor to Detect Test Inversions  

We instrument an external AES (Advanced Encryption Standard)
implementation in build-time mode to detect test inversions. The
instrumentation deploys inline monitors that duplicate all conditional
jumps in their successor blocks to report test inversions. We implement
the instrumentation as follows:

-   In BISM, we use built-in features to duplicate conditional jumps
    utilizing `insert` instrumentation method to add raw bytecode
    instructions. In particular, we use the `beforeInstruction`
    joinpoint to capture all conditional jumps. We extract the opcode
    from the static context object `Instruction` and we use the
    instrumentation method `insert` to duplicate the needed stack
    values. We then use the control-flow joinpoints `OnTrueBranchEnter`
    and `onFalseBranchEnter` to capture the blocks executing after the
    jump. Finally, at the beginning of these blocks, we utilize `insert`
    to duplicate conditional jumps.

-   In DiSL, we implement a custom `InstructionStaticContext` object to
    retrieve information from conditional jump instructions such as the
    index of a jump target and instruction opcode. Note, we use multiple
    `BytecodeMarker` snippets to capture all conditional jumps. To
    retrieve stack values, we use the dynamic context object. Finally,
    on successor blocks, we map opcodes to Java syntax to re-evaluate
    conditional jumps using switch statements.


# How to Run

The experiment can be run in load-time and build-time instrumentation.

* [buildtime](buildtime): to run in build-time instrumentation mode.
* [loadtime](loadtime): to run in loadtime-time instrumentation mode.
* [plot](plot): to generate the plots after running the experiments