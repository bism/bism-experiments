CRC: 0x72F64CB3  File: biographies.list  Date: Sat Dec 19 00:00:00 2015
BG: Richard Matthew Clark was born and raised in southeastern, NC, in a
BG: small town called, Chadbourn, NC.
BG: He has a tragic tale filled with emotion that becomes one with the
BG: canvas he paints. Clark has a way through any media to lead you into a
BG: world that traps your soul.
BG: His mother left him at six months old and his dad that's drinking almost
BG: destroyed the both of them.
BG: His mother came back into his life when he was twelve only to attempt to
BG: Art was his only outlet to express his emotion. His senior year in High
BG: School he excelled in the arts and was offered a full scholarship to
BG: the Parsons School of Art and the Art Institute, as well as, an
BG: internship with Marvel and Disney.
BG: The light at the end of the tunnel was soon dimmed as he had to put his
BG: dad in a rehab clinic and drop out of school to save their home.
BG: He saved their home but soon after his father was released from rehab
BG: his dad furious of being admitted into the treatment center, gave the
BG: home up any way and the two went their separate ways.
BG: and forever tried to find a way back to the path to use his art. He is
BG: the type of person that has faced trial after trial but no matter how
BG: many times he has been knocked to the ground he has always managed to
BG: summon the strength to stand back up and face whatever the powers that
BG: Trying to have a real family for once he married at 18 and had 2 sons by
BG: 21. His wife turned out to be just like his mom and ended up walking
BG: out on the family and never looking back.
BG: He was a single father and left the military where he would sell
BG: sketches and paint murals for income.
BG: 3 years past and he built a sign and graphics company and then married a
BG: female that tried to take what he had worked so hard to build. They had
BG: one child together and during the divorce proceeding the wife told the
BG: judge "Sir he can have the child I just want the money!" Clark threw
BG: the keys to her f