#!/bin/bash          
#  
toolarray=(original bism disl)
transformer=$"TransformerInlineMonitorTI"
N=100 # Number of runs
tsleep=5s
msleep=3s
pause=5m
#
#======== Preparation ========
mkdir -p original/ bism/ disl/
#
#Compile into original/
javac -sourcepath src src/aes/*.java -d original/
#Duplicate into bism/ and  disl/
cp -r original/* bism/
cp -r original/* disl/
#Instrument AES.class in bism/ using BISM tool
# javac -cp jar/bism.jar transformers/$transformer.java
# java -jar jar/bism.jar transformer=transformers/$transformer.class:target=bism/aes/AES.class
java -jar jar/bism.jar transformer=$transformer:target=bism/aes/AES.class
#Overwrite AES.class in disl/ by AES.class instrumented using DiSL tool
cp AES_disl.class disl/aes/AES.class 
#
#========== RunTime ==========
echo "runtime ..."
for tool in "${toolarray[@]}"
do
	 
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		for size in 1 2 4 8 16 32 64 128 256 
		do
	    output="$(java -cp $tool/ aes.Time "data/file_"$size"KB.txt" 2>&1)"
	    echo "P$size,$tool,$output" >> time.csv
	    sleep $tsleep
		done
	 
	done 
done
#
sleep $pause
#
#===== Memory Footprint =====
echo "memory footprint ..."
for tool in "${toolarray[@]}"
do
	 
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		for size in 1 2 4 8 16 32 64 128 256 
		do
	    output="$(java -cp $tool/ aes.Memory "data/file_"$size"KB.txt" 2>&1)"
	    echo "P$size,$tool,$output" >> memory.csv
	    sleep $msleep
		done
	 
	done 
done
#