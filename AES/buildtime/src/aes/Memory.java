package aes;

import java.util.LinkedList; 
import java.util.Queue; 
import java.lang.management.ManagementFactory;
import java.lang.management.GarbageCollectorMXBean;

public class Memory {

	public static long getCurrentlyUsedMemory() {
		return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() +
		ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed();
	}


	public static long getGcCount() {
		long sum = 0;
		for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
			long count = b.getCollectionCount();
			if (count != -1) { 
				sum +=  count; 
			}
		}
		return sum;
	}


	public static long getReallyUsedMemory() {
		long before = getGcCount();
		System.gc();
		while (getGcCount() == before);
		return getCurrentlyUsedMemory();
	}


	public static long getSettledUsedMemory() {
		long m;
		long m2 = getReallyUsedMemory();
		do {
			try{
				Thread.sleep(567);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} 
			m = m2;
			m2 = getReallyUsedMemory();
		} while (m2 < m);
		return m;
	}
	

	public static void main(String[] args){
		String KEY = "000102030405060708090a0b0c0d0e0f1011121314151617";	
		String FILE_NAME = args[0];  
		//Used Memory in Byte
		FileEncryption.encryptFile(FILE_NAME, KEY);
		System.out.println(getSettledUsedMemory());
	}
}


