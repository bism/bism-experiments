import ch.usi.dag.disl.annotation.After;
import ch.usi.dag.disl.annotation.AfterReturning;
import ch.usi.dag.disl.annotation.Before;
import ch.usi.dag.disl.marker.BodyMarker;
import ch.usi.dag.disl.marker.BytecodeMarker;
import ch.usi.dag.disl.marker.BasicBlockMarker;
import ch.usi.dag.disl.marker.PreciseBasicBlockMarker;
import ch.usi.dag.disl.staticcontext.InstructionStaticContext;
import ch.usi.dag.disl.staticcontext.InstructionFullStaticContext;
import ch.usi.dag.disl.staticcontext.BasicBlockStaticContext;
import ch.usi.dag.disl.dynamiccontext.DynamicContext;
import ch.usi.dag.disl.annotation.SyntheticLocal;
import ch.usi.dag.disl.annotation.ThreadLocal;

import java.util.*;

public class DiSLClass {
    
    @SyntheticLocal
    static int target = -1;
    @SyntheticLocal
    static Object op1 = null;
    @SyntheticLocal
    static Object op2 = null;
    @SyntheticLocal
    static int opcode = -1;


    @Before(marker = BasicBlockMarker.class, scope = "AES.*(..)")
    public static void afterbb(InstructionFullStaticContext bcsc, BasicBlockStaticContext bbsc) {

        if (opcode != -1) { //a branch occurred
            boolean tt = target == ((int) bcsc.getIndexOfRegionStart());
            boolean attack = false;
            switch (opcode) {

                case 164:             // IF_ICMPLE(164),

                    if (((int) op1 <= (int) op2) != tt) {
                        attack = true;
                    }
                    break;

                case 165:

                    if ((op1.equals(op2)) != tt) {
                        attack = true;
                    }
                    break;
                case 166:

                    if ((!op1.equals(op2)) != tt) {
                        attack = true;
                    }
                    break;

                case 159:

                    if (((int) op1 == (int) op2) != tt) {
                        attack = true;
                    }
                    break;

                case 160:

                    if (((int) op1 != (int) op2 != tt)) {
                        attack = true;
                    }
                    break;
                case 161:

                    if (((int) op1 < (int) op2 != tt)) {
                        attack = true;
                      
                    }
                    break;
                case 162:

                    if (((int) op1 >= (int) op2 != tt)) {
                        attack = true;
                    }
                    break;
                case 163:

                    if (((int) op1 > (int) op2 != tt)) {
                        attack = true;
                    }
                    break;

                case 153:          //  IFEQ(153),

                    if (((int) op1 == 0) != tt) {
                        attack = true;
                    }
                    break;
                case 154:


                    if (((int) op1 != 0) != tt) {
                        attack = true;
                    }
                    break;
                case 155:


                    if (((int) op1 < 0) != tt) {
                        attack = true;
                    }
                    break;
                case 156:


                    if (((int) op1 >= 0) != tt) {
                        attack = true;
                    }
                    break;
                case 157:


                    if (((int) op1 > 0) != tt) {
                        attack = true;
                    }
                    break;
                case 158:


                    if (((int) op1 <= 0) != tt) {
                        attack = true;
                    }
                    break;

                case 198:          // IFNULL(198),

                    if ((op1 == null != tt)) {
                        attack = true;
                    }
                    break;
                case 199:          // IFNONNULL(199),

                    if ((op1 != null) != tt) {
                        attack = true;
                    }
                    break;
                default:

                    break;

            }
            if(attack) {
                System.out.println("Test Inverison Attack!");
            }

            opcode = -1;
        }
    }


    @Before(marker = BytecodeMarker.class, order = 0, args = "IFEQ,IFGE,IFGT,IFLE,IFLT,IFNE", scope = "AES.*(..)")
    static void onDup(InstructionStaticContextFull bcsc, DynamicContext dc) {

        opcode = bcsc.getOpcode();
        op1 = dc.getStackValue(0, int.class);
        target = bcsc.getJumpTargetLabel();
    }

    @Before(marker = BytecodeMarker.class, order = 0, args = "IFNONNULL,IFNULL", scope = "AES.*(..)")
    static void onDupO(InstructionStaticContextFull bcsc, DynamicContext dc) {

        opcode = bcsc.getOpcode();
        op1 = dc.getStackValue(0, Object.class);
        target = bcsc.getJumpTargetLabel();
    }

    @Before(marker = BytecodeMarker.class, order = 0, args = "IF_ICMPEQ,IF_ICMPNE,IF_ICMPLT,IF_ICMPGE,IF_ICMPGT,IF_ICMPLE", scope = "AES.*(..)")
    static void onDup2(InstructionStaticContextFull bcsc, DynamicContext dc) {

        opcode = bcsc.getOpcode();
        op2 = dc.getStackValue(0, int.class);
        op1 = dc.getStackValue(1, int.class);
        target = bcsc.getJumpTargetLabel();
    }

    @Before(marker = BytecodeMarker.class, order = 0, args = "IF_ACMPEQ,IF_ACMPNE", scope = "AES.*(..)")
    static void onDup2Objects(InstructionStaticContextFull bcsc, DynamicContext dc) {

        opcode = bcsc.getOpcode();
        op2 = dc.getStackValue(0, Object.class);
        op1 = dc.getStackValue(1, Object.class);
        target = bcsc.getJumpTargetLabel();
    }

}
