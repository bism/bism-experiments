import ch.usi.dag.disl.staticcontext.InstructionStaticContext;

import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.JumpInsnNode;
import org.objectweb.asm.tree.LabelNode;

import ch.usi.dag.disl.util.AsmHelper.Insns;
import ch.usi.dag.disl.util.Insn;


public class InstructionFullStaticContext extends InstructionStaticContext {


    public int getIndexOfRegionStart () {
        final AbstractInsnNode startInsn = staticContextData.getRegionStart ();
        final InsnList insns = staticContextData.getMethodNode ().instructions;


        AbstractInsnNode insn = insns.getFirst ();

        int result = 0;
        while (insn != startInsn) {
            result += Insn.isVirtual (insn) ? 0 : 1;
            insn = insn.getNext ();
        }

        return result;
    }


    public int getIndexOfFollowingLeader () {
        final AbstractInsnNode startInsn = staticContextData.getRegionEnds().get(0).getNext ();
        final InsnList insns = staticContextData.getMethodNode ().instructions;

        AbstractInsnNode insn = insns.getFirst ();

        int result = 0;
        while (insn != startInsn) {
            result += Insn.isVirtual (insn) ? 0 : 1;
            insn = insn.getNext ();
        }

        return result;
    }



    public int getJumpTargetLabel () {

        final AbstractInsnNode insn = staticContextData.getRegionStart ();

        if (insn instanceof JumpInsnNode) {
            final JumpInsnNode ji = (JumpInsnNode) insn;
            return getIndexOfLabel (ji.label);
        } else {
            return -1;
        }

    }

    public int getIndexOfLabel (final LabelNode l) {

        final InsnList insns = staticContextData.getMethodNode ().instructions;

        AbstractInsnNode insn = insns.getFirst ();

        int result = 0;
        while (insn != l) {
            result += Insn.isVirtual (insn) ? 0 : 1;
            insn = insn.getNext ();
        }

        return result;
    }

}
