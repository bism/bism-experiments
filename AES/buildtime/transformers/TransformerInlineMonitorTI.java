
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.InstructionDynamicContext;
import inria.bism.transformers.staticcontext.BasicBlock;
import inria.bism.transformers.staticcontext.Instruction;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;


 

 
public class TransformerInlineMonitorTI extends Transformer {

    @Override
    public void beforeInstruction(Instruction ins, InstructionDynamicContext dc) {
        if (ins.isConditionalJump()) {
            if (ins.stackOperandsCountIfConditionalJump() == 1)
                insert(new InsnNode(Opcodes.DUP));
            else
                insert(new InsnNode(Opcodes.DUP2));
        }
    }

    @Override
    public void onTrueBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        LabelNode l_1 = new LabelNode();

        insert(new JumpInsnNode(jumpingBlock.getLastRealInstruction().opcode, l_1));

        insert(new FieldInsnNode(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
        insert(new LdcInsnNode("Test Inversion Attack!"));
        insert(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));
        insert(l_1);
    }

    @Override
    public void onFalseBranchEnter(BasicBlock jumpingBlock, InstructionDynamicContext dc) {
        LabelNode l_1 = new LabelNode();
        LabelNode l_2 = new LabelNode();

        insert(new JumpInsnNode(jumpingBlock.getLastRealInstruction().opcode, l_1));
        
        insert(new JumpInsnNode(Opcodes.GOTO, l_2));
        insert(l_1);
        insert(new FieldInsnNode(Opcodes.GETSTATIC, "java/lang/System", "out", "Ljava/io/PrintStream;"));
        insert(new LdcInsnNode("Test Inversion Attack!"));
        insert(new MethodInsnNode(Opcodes.INVOKEVIRTUAL, "java/io/PrintStream", "println", "(Ljava/lang/String;)V", false));
        insert(l_2);
    }
}
