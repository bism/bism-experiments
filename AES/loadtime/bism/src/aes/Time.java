package aes;
 
public class Time {

	public static void main(String[] args){
		String KEY = "000102030405060708090a0b0c0d0e0f1011121314151617";	
		String FILE_NAME = args[0]; 
		
		//Warm up phase
		int N = 10;
		for(int i = 0; i < 10; i++) {
			FileEncryption.encryptFile(FILE_NAME, KEY);
		}
		
		//Computation Time in nano-seconds
		long start = System.nanoTime(); 
		FileEncryption.encryptFile(FILE_NAME, KEY);
		System.out.println((System.nanoTime() - start));
	}
}