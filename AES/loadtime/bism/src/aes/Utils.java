package aes;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class Utils {

	public static String stringToHexadecimal(String str){
		StringBuffer sb = new StringBuffer();
		//Converting string to character array
		char ch[] = str.toCharArray();
		for(int i = 0; i < ch.length; i++) {
			String hexString = Integer.toHexString(ch[i]);
			sb.append(hexString);
		}
		String result = sb.toString();
		return result;
	}
	

	public static byte[] hexStringToByteArray(String str) {
		int len = str.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i/2] = (byte) ((Character.digit(str.charAt(i), 16) << 4)
			+ Character.digit(str.charAt(i+1), 16));
		}
		return data;
	}
	

	public static String bytesToHex(byte[] bytes) {
		char[] hexArray = "0123456789ABCDEF".toCharArray();
		char[] hexChars = new char[bytes.length * 2];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}


	public static String hexadecimalToString(String str){
		String result = new String();
		char[] charArray = str.toCharArray();
		for(int i = 0; i < charArray.length; i = i+2) {
			String st = ""+charArray[i]+""+charArray[i+1];
			char ch = (char)Integer.parseInt(st, 16);
			result = result + ch;
		}
		return result;
	}
	
	
	public static String encrypt(String plaintext, String key){
		//ECB-256
	    byte[] block = hexStringToByteArray(stringToHexadecimal(plaintext));
	    byte[] result = AES.encryptBlockECB(block, hexStringToByteArray(key), 4, 14, 8);
	    return bytesToHex(result);
	}
	
	
	public static String decrypt(String ciphertext, String key){
		//ECB-256
		byte[] block = hexStringToByteArray(ciphertext);
		byte[] result = AES.decryptBlockECB(block, hexStringToByteArray(key), 4, 14, 8);
		return hexadecimalToString(bytesToHex(result));
	}	
	
	
	public static String generateSecretKey() {
		KeyGenerator keyGen = null;
		try {
			keyGen = KeyGenerator.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		keyGen.init(256);
		SecretKey secretKey = keyGen.generateKey();		
		String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
		String hexKey = stringToHexadecimal(encodedKey);
		return hexKey.substring(0, 64);
	}
}