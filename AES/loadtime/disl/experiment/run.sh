
toolarray=(disl)
N=100
tsleep=5s
msleep=3s
pause=2s

#========== RunTime ==========
echo "runtime ..."
for tool in "${toolarray[@]}"
do
	
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		for size in 1 2 4 8 16 32 64 128 256 
		do
	    output="$(../bin/disl.py -d ../output -s_nodynamicbypass -s_noexcepthandler -- build/inst.jar -jar time.jar "data/file_"$size"KB.txt" 2>&1)"
	    echo "P$size,$tool,$output" >> time.csv
	    sleep $tsleep
		done
	
	done 
done
sleep $pause
#
#===== Memory Footprint =====
echo "memory footprint ..."
for tool in "${toolarray[@]}"
do
	
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		for size in 1 2 4 8 16 32 64 128 256 
		do
	     output="$(../bin/disl.py -d ../output -s_nodynamicbypass -s_noexcepthandler -- build/inst.jar -jar memory.jar "data/file_"$size"KB.txt" 2>&1)"
	     echo "P$size,$tool,$output" >> memory.csv
	    sleep $msleep
		done
	done 
done

 