## Inline Monitor to Detect Test Inversions  

We instrument an external AES (Advanced Encryption Standard)
implementation in build-time mode to detect test inversions. The
instrumentation deploys inline monitors that duplicate all conditional
jumps in their successor blocks to report test inversions. We implement
the instrumentation as follows:

-   In BISM, we use built-in features to duplicate conditional jumps
    utilizing `insert` instrumentation method to add raw bytecode
    instructions. In particular, we use the `beforeInstruction`
    joinpoint to capture all conditional jumps. We extract the opcode
    from the static context object `Instruction` and we use the
    instrumentation method `insert` to duplicate the needed stack
    values. We then use the control-flow joinpoints `OnTrueBranchEnter`
    and `onFalseBranchEnter` to capture the blocks executing after the
    jump. Finally, at the beginning of these blocks, we utilize `insert`
    to duplicate conditional jumps.

-   In DiSL, we implement a custom `InstructionStaticContext` object to
    retrieve information from conditional jump instructions such as the
    index of a jump target and instruction opcode. Note, we use multiple
    `BytecodeMarker` snippets to capture all conditional jumps. To
    retrieve stack values, we use the dynamic context object. Finally,
    on successor blocks, we map opcodes to Java syntax to re-evaluate
    conditional jumps using switch statements.


### How to Run

1. Java Source code is in  **src**/ 

2. The experiment can be run using command: **bash run.sh**

  The script run.sh compiles the source code, perform instrumentation, and computes runtime and memory footprint.

3. Only AES.class get instrumented by deploying a monitor that detect test inversions

4. Instrumentation for BISM is done by applying the transformer TransformerInlineMonitorTI using jar/bism.jar

  TransformerInlineMonitorTI is built-in bism.jar

  *Note,* transformers/TransformerInlineMonitorTI.java *presents the corresponding instrumentation logic*

5. Instrumentation for DiSL is done (while disabling exception handler) based on 

  transformers/DiSLClass.java and transformers/InstructionFullStaticContext.java

  *Note,* AES_disl.class *is AES instrumented using DiSL*

6. Plaintext data (to be encrypted using AES) are in data/

7. Results are written in results/

8. A copy of resulting bytecode (.class) files is in:
    - original.zip (original classes), 

    - bism.zip (AES.class is instrumented by BISM), and 

    - disl.zip (AES.class is instrumented by DiSL)
