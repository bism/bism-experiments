CRC: 0x72F64CB3  File: biographies.list  Date: Sat Dec 19 00:00:00 2015
BG: Richard Matthew Clark was born and raised in southeastern, NC, in a
BG: small town called, Chadbourn, NC.
BG: He has a tragic tale filled with emotion that becomes one with the
BG: canvas he paints. Clark has a way through any media to lead you into a
BG: world that traps your soul.
BG: His mother left him at six months old and his dad that's drinking almost
BG: destroyed the both of them.
BG: His mother came back into his life when he was twelve only to attempt to
BG: Art was his only outlet to express his emotion. His senior year in High
BG: School he excelled in the arts and was offered a full scholarship to
BG: the Parsons School of Art and the Art Institute, as well as, an
BG: internship with Marvel and Disney.
BG: The light at the end of the tunnel was soon dimmed as he had to put his
BG: dad in a rehab clinic and drop out of school to save their home.
BG: He saved their home but soon after his father was released from rehab
BG: his dad furious 