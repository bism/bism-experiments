#!/bin/bash          
#  
toolarray=(original)
N=100 # Number of runs
tsleep=5s
msleep=3s
pause=5m
#
#======== Preparation ========
mkdir -p original/ 
#
#Compile into original/
javac -sourcepath src src/aes/*.java -d original/
#
#========== RunTime ==========
echo "runtime ..."
for tool in "${toolarray[@]}"
do
	 
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		for size in 1 2 4 8 16 32 64 128 256 
		do
	    output="$(java -cp $tool/ aes.Time "data/file_"$size"KB.txt" 2>&1)"
	    echo "P$size,$tool,$output" >> time.csv
	    sleep $tsleep
		done
	 
	done 
done
#
sleep $pause
#
#===== Memory Footprint =====
echo "memory footprint ..."
for tool in "${toolarray[@]}"
do
	 
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		for size in 1 2 4 8 16 32 64 128 256 
		do
	    output="$(java -cp $tool/ aes.Memory "data/file_"$size"KB.txt" 2>&1)"
	    echo "P$size,$tool,$output" >> memory.csv
	    sleep $msleep
		done
	 
	done 
done
#