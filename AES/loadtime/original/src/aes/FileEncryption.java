package aes;

import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.ArrayList;

public class FileEncryption {
	
	public static String EncryptECB(String plaintext, String key){
	    byte[] cipherkey = Utils.hexStringToByteArray(key);
	    byte[] block = Utils.hexStringToByteArray(Utils.stringToHexadecimal(plaintext));
	    byte[] result = AES.encryptBlockECB(block, cipherkey, 4, 12, 6);
	    return Utils.bytesToHex(result);
	}

	
	public static String DecryptECB(String ciphertext, String key){
		byte[] cipherkey = Utils.hexStringToByteArray(key);
		byte[] block = Utils.hexStringToByteArray(ciphertext);
		byte[] result = AES.decryptBlockECB(block, cipherkey, 4, 12, 6);
		return Utils.hexadecimalToString(Utils.bytesToHex(result));
	}


    public static void encryptFile(String filename, String key){
    	ArrayList<String> plaintext = new ArrayList<>();
    	ArrayList<String> chipertext = new ArrayList<>();
    	String line;
    	//
    	try { 
    		File file = new File(filename);     
	    	FileReader reader = new FileReader(file);    
	    	BufferedReader buffer = new BufferedReader(reader);  
	    	while((line = buffer.readLine())!=null) { 
		    	chipertext.add(EncryptECB(line, key));
	    	}  
	    	reader.close();  

	    	for(int i=0; i < chipertext.size(); i++){
	    		plaintext.add(DecryptECB(chipertext.get(i), key));
	    	}
    	}  
    	catch(IOException e) {  
    		e.printStackTrace();  
    	}
    }
}       