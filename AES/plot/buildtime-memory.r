library(ggplot2)
library(dplyr)

df <- read.csv("buildtime-memory.csv", dec=".")

df$tool <- factor(df$tool,levels=c("original", "disl", "bism"))
df$benchmark <- factor(df$benchmark)

df_fixed <- df %>% 
  group_by(benchmark, tool) %>%
  summarise(
    n=n(),
    mean=mean(execution),
    sd=sd(execution)
  ) %>%
  mutate(se=sd/sqrt(n))

  ggplot(df_fixed) + 
  geom_bar(aes(x=benchmark,y=mean, fill=tool), stat="identity", width=.5, position = position_dodge(0.6)) +
  geom_errorbar(aes(x=benchmark, fill=tool, ymin=mean-sd, ymax=mean+sd), width=.35, position = position_dodge(0.6), alpha=0.7) +
  scale_fill_manual(values = c( "#FDCC8a", "#fc8d59","#7f0000")) +
  theme_minimal() + 
  ylab(element_blank()) +
  xlab(element_blank()) +
  guides(fill=guide_legend(title=element_blank())) +
  theme(legend.position="top", panel.grid.major.x = element_blank(), text = element_text(size=20)) +
  scale_x_discrete(labels = c(expression(2^0), expression(2^1), expression(2^2), expression(2^3), expression(2^4), expression(2^5), expression(2^6), expression(2^7), expression(2^8))) +
  scale_y_continuous(minor_breaks=c(1000,3000,5000,7000)) +
  ggsave("buildtime-memory.pdf")   


