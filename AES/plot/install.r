# R package installer

local({r <- getOption("repos")
       r["CRAN"] <- "http://cran.r-project.org" 
       options(repos=r)
})

# List needed packages
pkgs <- c("ggplot2","magrittr", "dplyr","scales")

install.packages(pkgs)

