CRC: 0x72F64CB3  File: biographies.list  Date: Sat Dec 19 00:00:00 2015
BG: Richard Matthew Clark was born and raised in southeastern, NC, in a
BG: small town called, Chadbourn, NC.
BG: He has a tragic tale filled with emotion that becomes one with the
BG: canvas he paints. Clark has a way through any media to lead you into a
BG: world that traps your soul.
BG: His mother left him at six months old and his dad that's drinking almost
BG: destroyed the both of them.
BG: His mother came back into his life when he was twelve only to attempt to
BG: Art was his only outlet to express his emotion. His senior year in High
BG: School he excelled in the arts and was offered a full scholarship to
BG: the Parsons School of Art and the Art Institute, as well as, an
BG: internship with Marvel and Disney.
BG: The light at the end of the tunnel was soon dimmed as he had to put his
BG: dad in a rehab clinic and drop out of school to save their home.
BG: He saved their home but soon after his father was released from rehab
BG: his dad furious of being admitted into the treatment center, gave the
BG: home up any way and the two went their separate ways.
BG: and forever tried to find a way back to the path to use his art. He is
BG: the type of person that has faced trial after trial but no matter how
BG: many times he has been knocked to the ground he has always managed to
BG: summon the strength to stand back up and face whatever the powers that
BG: Trying to have a real family for once he married at 18 and had 2 sons by
BG: 21. His wife turned out to be just like his mom and ended up walking
BG: out on the family and never looking back.
BG: He was a single father and left the military where he would sell
BG: sketches and paint murals for income.
BG: 3 years past and he built a sign and graphics company and then married a
BG: female that tried to take what he had worked so hard to build. They had
BG: one child together and during the divorce proceeding the wife told the
BG: judge "Sir he can have the child I just want the money!" Clark threw
BG: the keys to her from across the court room and kept his 3 sons
BG: He lost everything again but then kept the thing that mattered
BG: With just an airbrush left he went to the beaches of the Carolina's and
BG: started painting t-shirts for money. This fine artist that seemed to
BG: channel the spirits of the masters had been reduced to doing whatever
BG: He found himself working night and day to provide for everyone and with
BG: the street smarts he possessed he managed to turn money over and open
BG: an airbrush shop in the mall. Three years past and he met a bi racial
BG: girl that was born and raised in Germany. They dated for a while and
BG: she seemed to be the only one that could tame the battle tossed artist.
BG: They were married in 2005 and now Clark finally has his family and now
BG: Although, Clark, has been through so much he always remains positive and
BG: humble and true to his southern charm. His work is able to captivate
BG: people from all walks of life and crosses over to all races, religions
BG: and backgrounds. His art is now world wide and his clients include
BG: everyone from the Ruff Ryderz, Warren Sapp and the Hollywood Elite.
BG: Mr. Clark does everything from acting, producing, writing screenplays,
BG: set painting, special effects make up, music; and even owns custom body
BG: shops and mall stores, as well as, his own Production Company.
BG: Born and raised in the Carolina's of the United States he is a hardened
BG: and battle tossed soul that still displays the characteristics of a
BG: leader. One that does not drink or use drugs; he stands as a knight
BG: making sure others do not have to struggle and feel the pain that he
BY: Tesha Dockery "germanpheonix@yahoo.com"
SP: * 'Tene Clark' (17 November 2005 - present)
NM: "Thank You" credits for All Things Must Pass
TR: * This name represents the individual "Thank You" credits for All Things
TR:   Must Pass (2015) since the list is too large and time consuming to
TR:   represent fully here, sorry. Please visit the official site for the
DB: 28 April 1966, Los Angeles, California, USA
TR: * Well known as rapper Too Short
TR: * Nephew of 'Beverly Ransom' (qv).
TR: * Brother of 'Wayne Loc' (qv).
TR: * Began his career in entertainment promoting and managing rap artists
TR:   before eventually becoming a rap artist himself.
TR: * His father was stationed at Fort Benning, Georgia, at the time of his
TR: * Was born at Martin Army Hospital in Fort Benning, Georgia.
TR: * Growing up an avid zombie movie buff, he got his first ever movie role
TR:   in the independent horror film, "Night of the Jackals" (2009), which
TR:   featured zombie-like creatures known as "Jackals".
QU: * Perseverance is the key to success.
BG: Born in Iraq, Ja'far moved with his family to other parts of the Arab
BG: world before settling in London in the mid 1980s. While reading social
BG: sciences at the University of London, Ja'far began to assist in
BG: independent TV and radio productions in London. After attending courses
BG: in film production at London's Lux Centre, Hoxton, he began to write
BG: film criticism for the Arabic daily 'Al-Hayat" newspaper, and later for
BG: the Paris-based "Cinema" magazine.
BG: After completing an MA in 20th Century Historical Studies and
BG: "discovering" the work of Frank Capra, Sergei Eisenstein and Dziga
BG: Vertov within the context of "political cinema", Ja'far began an MA in
BG: Film Studies, after the completion of which he worked on a PhD thesis
BG: During his film studies, he worked on a series of TV pilots and
BG: programmes for small community-based TV stations, making "Test Drive"
BG: (16mm) in 1999, and shooting the short film "Eyes Wide Open" (16mm)
BG: which was finally edited in 2005, and making "A Two Hour Delay" (BW
BG: 16mm) in 2000, which screened at a festival for independent cinema in
BG: Ja'far has also helped organize film festivals in London and the Gulf,
BG: most notably, programming assistant at the Raindance Film Festival in
BG: Mesocafe is Ja'far's directorial feature debut.
BG: Diego Pecori alias Diego Dada is graduated in 2006 at the Art High
BG: School "Virgilio" in Empoli in the section of Photography and Film. Is
BG: currently a student at the Faculty of Arts and Humanities of the
BG: University of Florence, Bachelor Degree in "The Arts, music and
BG: entertainment". Between 2005 and 2010 he made several short films, some
BG: of which have received awards and great attention from the public on
DB: 1971, Montpellier, H�rault, France
DB: 14 February 1922, New York City, New York, USA
DD: 21 February 1982, Los Angeles, California, USA (cancer)
BG: Murray the K was born Murray Kaufman in New York, New York, on 14
BG: February 1922. After an early career as a song-plugger, he moved into
BG: radio and in 1958 joined 1010 WINS. He remained there for seven years,
BG: becoming the most popular New York radio DJ. He was an early supporter
BG: of singer Bobby Darin, inspired and then 'broke' his hit single,
BG: 'Splish-Splash', and made a guest appearance on his "This is Your Life"
BG: In 1964, he was one of the first Americans to interview The Beatles,
BG: firstly by phone, later joining them in their hotel suite. From then on
BG: he acted as their "Mr. Fix-it", arranging for them to visit all the
BG: best clubs and restaurants. He also championed their records and for a
BG: while, he dubbed himself "the fifth Beatle" and became a trusted friend
BG: of the group during their American tours, though not of manager Brian
BG: Epstein, who apparently resented his considerable influence.
BG: He left WINS in 1965 and later resurfaced as a presenter on WOR-FM - the
BG: Married six times, he died of cancer on 21 February 1982, in Los
SP: * 'Jacklyn Zeman' (qv) (14 February 1979 - 1981) (divorced)
TR: * Legendary disk jockey who made his name at WINS (New York) in the 1950s
TR:   and 60s; a pioneer of progressive radio at WOR-FM (New York) in 1966.
TR: * Biography in: "The Scribner Encyclopedia of American Lives". Volume One,
TR:   1981-1985, pages 443-444. New York: Charles Scribner's Sons, 1998.
TR: * Father of 'Peter Altschuler'.
TR: * In 1963 took his 1010WINS NYC Radio show to the High Schools in the New
TR:   York City area as part of a "