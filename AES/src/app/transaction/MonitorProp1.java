package  transaction;

public class MonitorProp1 {

	//Need access to database <uid, country>. type:1 for gold, 2 for silver, and 3 for normal.

    public static int eventsCounter = 0;

	public static void userType(Integer uid, int type) {
		// System.out.println(uid + " " + type);
        eventsCounter++;
	}

	public static void printCounter() {
		System.out.println(eventsCounter);
	}	

}

