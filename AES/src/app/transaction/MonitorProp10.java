package  transaction;

public class MonitorProp10 {

    public static int eventsCounter = 0;

	public static void login(Integer uid) {
		// System.out.println("login "+ uid);
        eventsCounter++;	
    }

	public static void logout(Integer uid) {
		// System.out.println("logout "+ uid);
        eventsCounter++;	
    }
	
	public static void payToExternal(Integer uid) {
		// System.out.println("pay "+ uid);
        eventsCounter++;	
	}

	public static void transferToOtherAccount(Integer uid) {
		// System.out.println("transfer "+ uid);
        eventsCounter++;	
	}

	public static void transferOwnAccounts(Integer uid) {
		// System.out.println("move "+ uid);
        eventsCounter++;	
	}

	public static void printCounter() {
		System.out.println(eventsCounter);
	}	
}

