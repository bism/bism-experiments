package  transaction;

public class MonitorProp2 {
	
    public static int eventsCounter = 0;

	public static void initialise() {
		// System.out.println("initialise");
        eventsCounter++;
	}

	public static void login() {
		// System.out.println("login");
        eventsCounter++;
	}

	public static void printCounter() {
		System.out.println(eventsCounter);
	}	
}

