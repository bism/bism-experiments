package  transaction;

public class MonitorProp3 {

	//Need access to database <account_number, balance>. 
	//Two different users may have same account number

    public static int eventsCounter = 0;

	public static void payToExternal(Integer uid, String account_number) {
		// System.out.println(uid + " Ext. " + account_number);
        eventsCounter++;
	}

	public static void transferToOtherAccount(Integer uid, String account_number) {
		// System.out.println(uid + " Other " + account_number);
        eventsCounter++;
	}

	public static void transferOwnAccounts(Integer uid, String account_number) {
		// System.out.println(uid + " Own "+ uid);
        eventsCounter++;
	}

	public static void printCounter() {
		System.out.println(eventsCounter);
	}	
}

