package  transaction;

public class MonitorProp5 {

    public static int eventsCounter = 0;

	public static void disableUser(Integer uid) {
		// System.out.println("Disable "+ uid);
        eventsCounter++;	
	}

	public static void activateUser(Integer uid) {
		// System.out.println("Active "+ uid);
        eventsCounter++;	
	}

	public static void payToExternal(Integer uid) {
		// System.out.println("Ext. "+ uid);
        eventsCounter++;	
	}

	public static void transferToOtherAccount(Integer uid) {
		// System.out.println("Other "+ uid);
        eventsCounter++;	
	}

	public static void transferOwnAccounts(Integer uid) {
		// System.out.println("Own "+ uid);
        eventsCounter++;	
	}
	
	public static void printCounter() {
		System.out.println(eventsCounter);
	}	

}

