package  transaction;

public class MonitorProp6 {

    public static int eventsCounter = 0;

	public static void greylistUser(Integer uid) {
		// System.out.println("Grey "+ uid);
        eventsCounter++;	
	}

	public static void depositFromExternal(Integer uid) {
		// System.out.println("Deposit "+ uid);
        eventsCounter++;	
	}

	public static void whitelistUser(Integer uid) {
		// System.out.println("White "+ uid);
        eventsCounter++;	
	}
	
	public static void printCounter() {
		System.out.println(eventsCounter);
	}	
}

