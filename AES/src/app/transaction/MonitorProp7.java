package  transaction;

public class MonitorProp7 {

    public static int eventsCounter = 0;

	public static void login(Integer sid) {
		// System.out.println("Login "+ sid);
        eventsCounter++;	
	}

	public static void requestAccount(Integer sid) {
		// System.out.println("Request "+ sid);
        eventsCounter++;	
	}

	public static void logout(Integer sid) {
		// System.out.println("Logout "+ sid);
        eventsCounter++;	
	}
	
	public static void printCounter() {
		System.out.println(eventsCounter);
	}	
}

