package  transaction;
 
public class Time {

	public static void main(String[] args){
		if(args.length > 0){
			int scenario = Integer.parseInt(args[0]);
			
			//Warm up phase		
			int N = 10;
			for(int i = 0; i < N; i++) {
				switch(scenario){
					case 1: Scenario1.run(); break;
					case 2: Scenario2.run(); break;
					case 3: Scenario3.run(); break;
					case 4: Scenario4.run(); break;	
					case 5: Scenario5.run(); break;	
					case 6: Scenario6.run(); break;	
					case 7: Scenario7.run(); break;	
					case 8: Scenario8.run(); break;	
					case 9: Scenario9.run(); break;	
					case 10: Scenario10.run(); break;	
					default: System.out.println("Requested scenario is not defined.");
				}
			}
		
			long start = System.nanoTime(); 
				switch(scenario){
					case 1: Scenario1.run(); break;
					case 2: Scenario2.run(); break;
					case 3: Scenario3.run(); break;
					case 4: Scenario4.run(); break;	
					case 5: Scenario5.run(); break;	
					case 6: Scenario6.run(); break;	
					case 7: Scenario7.run(); break;	
					case 8: Scenario8.run(); break;	
					case 9: Scenario9.run(); break;	
					case 10: Scenario10.run(); break;	
					default: System.out.println("Requested scenario is not defined.");
				}
			long end = System.nanoTime(); 	 
			System.out.println((end - start));
		}
	}
}