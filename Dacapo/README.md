# DaCapo Benchmarks 

We compare BISM, DiSL, and AspectJ in a general runtime verification
scenario. We instrument the benchmarks in the DaCapo
suite (dacapo-9.12-bach), to monitor the classical
**HasNext**, **UnSafeIterator**, and **SafeSyncMap** properties. We
only target the packages specific to each benchmark and do not limit our
scope to `java.util` types; instead, we match freely by type and method
name. We implement an external monitor library with stub methods that
only count the number of received events.

We implement the instrumentation as follows:

-   In BISM, we use the static context provided at method call
    joinpoints to filter methods by their names and owners. To access
    the method calls' receivers and results, we utilize the methods
    available in dynamic contexts.

-   In DiSL, we implement custom Markers to capture the needed method
    calls and use argument processors and dynamic context objects to
    access dynamic values.

-   In AspectJ, we use the call pointcut, type pattern matching and
    joinpoint static information to capture method calls and write
    custom advices that invoke the monitor.


# How to Run

The experiment can be run in load-time and build-time instrumentation.

* [buildtime](buildtime): to run in build-time instrumentation mode.
* [loadtime](loadtime): to run in loadtime-time instrumentation mode.
* [plot](plot): to generate the plots after running the experiments