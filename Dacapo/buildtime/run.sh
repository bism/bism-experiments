start=1
end=100
sleep=3


bencharray=(avrora batik fop h2 pmd sunflow xalan)
 
for j in "${bencharray[@]}"
do
	for ((i=start; i<=end; i++))
		do

			if [ "$j" = "fop" ]; then
				
				java -jar  -noverify dacapo.jar $j   -c MyCallback  2>&1 | tee temp.txt >> log.txt
			else
				java -jar dacapo.jar $j   -c MyCallback  2>&1 | tee temp.txt >> log.txt
			fi		
			
			numb=$(grep   -E -e '[0-9]+\.[0-9]+ KB' -o temp.txt)
			numb=${numb%" KB"}
			echo "$j,Original,$numb" >> memory.csv

			numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
			numb=${numb%" msec"}
			echo "$j,Original,$numb" >> time.csv



		sleep $sleep
	done

		
	for ((i=start; i<=end; i++))
		do
			if [ "$j" = "fop" ]; then
				
               java  -noverify -jar dacapoBISM.jar $j  -c MyCallback 2>&1 | tee temp.txt >> log.txt
            else
               java -jar dacapoBISM.jar $j -c MyCallback 2>&1 | tee temp.txt >> log.txt
            fi
			 
			numb=$(grep   -E -e '[0-9]+\.[0-9]+ KB' -o temp.txt)
			numb=${numb%" KB"}
			echo "$j,BISM,$numb" >> memory.csv

			numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
			numb=${numb%" msec"}
			echo "$j,BISM,$numb" >> time.csv

			sleep $sleep
	done
 
 

 	for ((i=start; i<=end; i++))
		do
			if [ "$j" = "fop" ]; then
			
				java -jar  -noverify dacapoDiSL.jar $j -c MyCallback 2>&1 | tee temp.txt >> log.txt
			else
				java  -jar dacapoDiSL.jar $j -c MyCallback 2>&1 | tee temp.txt >> log.txt
			fi	


      		numb=$(grep   -E -e '[0-9]+\.[0-9]+ KB' -o temp.txt)
			numb=${numb%" KB"}
			echo "$j,DiSL,$numb" >> memory.csv

			numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
			numb=${numb%" msec"}
			echo "$j,DiSL,$numb" >> time.csv


	 
			sleep $sleep
	done
	
	 for ((i=start; i<=end; i++))
		do

			if [ "$j" = "fop" ]; then
				
				java -jar  -noverify dacapoAspectJ.jar $j -c MyCallback  2>&1 | tee temp.txt >> log.txt
			else
				java -jar dacapoAspectJ.jar $j  -c MyCallback 2>&1 | tee temp.txt >> log.txt
			fi		
				
			numb=$(grep   -E -e '[0-9]+\.[0-9]+ KB' -o temp.txt)
			numb=${numb%" KB"}
			# results+=("$numb")
			echo "$j,AspectJ,$numb" >> memory.csv

			numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
			numb=${numb%" msec"}
			# results+=("$numb")
			echo "$j,AspectJ,$numb" >> time.csv


			sleep $sleep
	done 	

done
