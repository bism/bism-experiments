
start=1
end=100

bencharray=(avrora batik fop h2 pmd sunflow xalan)

for j in "${bencharray[@]}"
do
	for i in {start..end}
	do
		java  -javaagent:aspectjweaver-1.9.4.jar   -jar dacapoAspect.jar $j -c MyCallback 2>&1 | tee temp.txt >> log.txt
	
		numb=$(grep   -E -e '[0-9]+\.[0-9]+ KB' -o temp.txt)
		numb=${numb%" KB"}
		echo "$j,AspectJ,$numb" >> memory.csv


		numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
		numb=${numb%" msec"}
		echo "$j,AspectJ,$numb" >> time.csv

	sleep 3
	done
done
 
 
 













