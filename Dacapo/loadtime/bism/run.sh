
start=1
end=100

bencharray=(avrora batik fop h2 pmd sunflow xalan)

for j in "${bencharray[@]}"
do
	for i in {start..end}
	do
	
		if [ "$j" = "avrora" ]; then
			java  -javaagent:bism.jar=transformer=HasNextTransformer:scope=avrora.* -jar dacapo.jar avrora -c MyCallback 2>&1 | tee temp.txt >> log.txt
		elif [ "$j" = "batik" ]; then
			java -javaagent:bism.jar=transformer=HasNextTransformer:scope=org.apache.batik.* -jar dacapo.jar batik  -c MyCallback 2>&1 | tee temp.txt >> log.txt
		elif [ "$j" = "fop" ]; then
			java -noverify -javaagent:bism.jar=transformer=HasNextTransformer:scope=org.apache.fop.* -jar dacapo.jar fop  -c MyCallback 2>&1 | tee temp.txt >> log.txt
		elif [ "$j" = "h2" ]; then
			java -javaagent:bism.jar=transformer=HasNextTransformer:scope=org.h2.* -jar dacapo.jar h2  -c MyCallback 2>&1 | tee temp.txt >> log.txt
		elif [ "$j" = "pmd" ]; then
			java -javaagent:bism.jar=transformer=HasNextTransformer:scope=net.* -jar dacapo.jar pmd  -c MyCallback 2>&1 | tee temp.txt >> log.txt
		elif [ "$j" = "sunflow" ]; then
			java -javaagent:bism.jar=transformer=HasNextTransformer:scope=org.sunflow.* -jar dacapo.jar sunflow  -c MyCallback 2>&1 | tee temp.txt >> log.txt
		elif [ "$j" = "xalan" ]; then
			java -javaagent:bism.jar=transformer=HasNextTransformer:scope=org.apache.xalan.* -jar dacapo.jar xalan  -c MyCallback 2>&1 | tee temp.txt >> log.txt
		fi


	
		numb=$(grep   -E -e '[0-9]+\.[0-9]+ KB' -o temp.txt)
		numb=${numb%" KB"}
		echo "$j,BISM,$numb" >> memory.csv


		numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
		numb=${numb%" msec"}
		echo "$j,BISM,$numb" >> time.csv

	sleep 3
	done
done
 
 












