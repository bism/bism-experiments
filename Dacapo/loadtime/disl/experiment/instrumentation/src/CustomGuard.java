import ch.usi.dag.disl.annotation.GuardMethod; 


public class CustomGuard {

	@GuardMethod
	public static boolean isApplicable(HarnessContext dlc) {
		return !dlc.isConstructor() ;
	}
}
