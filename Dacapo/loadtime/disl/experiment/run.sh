

start=1
end=100

bencharray=(avrora batik fop h2 pmd sunflow xalan)

for j in "${bencharray[@]}"
do
	for i in {start..end}
	do
		../bin/disl.py -d ../output  -s_noexcepthandler -- instrumentation/build/$j-inst.jar -jar dacapo.jar $j -c  MyCallback  2>&1 | tee temp.txt >> log.txt
	 
 
		numb=$(grep   -E -e '[0-9,]+ msec' -o temp.txt)
		numb=${numb%" msec"}
		echo "$j,DiSL,$numb" >> time.csv

	sleep 3
	done
done
 
 