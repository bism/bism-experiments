library(ggplot2)
library(magrittr)
library(dplyr)

df <- read.csv("loadtime-memory.csv")
df$tool <- factor(df$tool, 
                  levels=c("Original", "AspectJ", "BISM"))

df_fixed <- df %>% 
  group_by(benchmark, tool) %>%
  summarise(
    n=n(),
    mean=mean(execution/1000),
    sd=sd(execution/1000)
  ) %>%
  mutate(se=sd/sqrt(n))

ggplot(df_fixed) +
  geom_bar(aes(x=benchmark,y=mean, fill=tool), stat="identity", width=.5, position = position_dodge(0.6)) +
  geom_errorbar(aes(x=benchmark, fill=tool, ymin=mean-sd, ymax=mean+sd), width=.35, 
                position = position_dodge(0.6), alpha=0.7) +
  ##scale_fill_grey() +
  #scale_color_manual(values = c("lightgray", "darkgray", "black"))+
  scale_fill_brewer(palette = "OrRd") +
  theme_minimal() +
  ylab(element_blank()) +
  xlab(element_blank()) +
  guides(fill=guide_legend(title=element_blank())) +
  theme(legend.position="top" ,panel.grid.major.x = element_blank(),text = element_text(size=20)) +
  ggsave("loadtime-memory.pdf")   