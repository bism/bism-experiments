/*
 * Copyright (c) 2006, 2009 The Australian National University.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Apache License v2.0.
 * You may obtain the license at
 * 
 *    http://www.opensource.org/licenses/apache2.0.php
 */
import org.dacapo.harness.Callback;
import org.dacapo.harness.CommandLineArgs;
import mycustommonitors.HasNextRuntimeMonitor;

import java.util.LinkedList; 
import java.util.Queue; 
import java.lang.management.ManagementFactory;
import java.lang.management.GarbageCollectorMXBean;

/**
 * date:  $Date: 2009-12-24 11:19:36 +1100 (Thu, 24 Dec 2009) $
 * id: $Id: MyCallback.java 738 2009-12-24 00:19:36Z steveb-oss $
 */
public class MyCallback extends Callback {

	public MyCallback(CommandLineArgs args) {
		super(args);
	}

	/* Immediately prior to start of the benchmark */
	@Override
	public void start(String benchmark) {
		System.err.println("my hook starting " + (isWarmup() ? "warmup " : "") + benchmark);
		super.start(benchmark);
	};

	/* Immediately after the end of the benchmark */
	@Override
	public void stop(long duration) {
		super.stop(duration);
		System.err.println("my hook stopped " + (isWarmup() ? "warmup" : ""));
		System.err.flush();
	};

	@Override
	public void complete(String benchmark, boolean valid) {
		super.complete(benchmark, valid);
		
		long m = getSettledUsedMemory();
		System.out.println("Used Memory: " + m/1000.0 + " KB");

		System.out.println("Events: " + HasNextRuntimeMonitor.eventsCounter);
		System.out.println("Null: " + HasNextRuntimeMonitor.nullCounter);
		System.err.println("my hook " + (valid ? "PASSED " : "FAILED ") + (isWarmup() ? "warmup " : "") + benchmark);
		
		System.err.flush();
	};


	public static long getCurrentlyUsedMemory() {
		return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() +
		ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed();
	}


	public static long getGcCount() {
		long sum = 0;
		for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
			long count = b.getCollectionCount();
			if (count != -1) { 
				sum +=  count; 
			}
		}
		return sum;
	}


	public static long getReallyUsedMemory() {
		long before = getGcCount();
		System.gc();
		while (getGcCount() == before);
		return getCurrentlyUsedMemory();
	}


	public static long getSettledUsedMemory() {
		long m;
		long m2 = getReallyUsedMemory();
		do {
			try{
				Thread.sleep(567);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} 
			m = m2;
			m2 = getReallyUsedMemory();
		} while (m2 < m);
		return m;
	}


}
