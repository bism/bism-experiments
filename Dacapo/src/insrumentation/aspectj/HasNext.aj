
public aspect HasNext {

	pointcut HasNext_updateList(Object i) : call(*  add*(..) ) && target(i) 
	&& if( (
			thisJoinPointStaticPart.getSignature().getDeclaringTypeName().contains("List"))
 			 && thisJoinPointStaticPart.getSignature().getName().contains("add"));

	before(Object i): HasNext_updateList(i) {

		mycustommonitors.HasNextRuntimeMonitor.updatedList(i);

	}

	pointcut HasNext_mapPut(Object i) : call(* put*(..))  && target(i) 
	&& if( (
			thisJoinPointStaticPart.getSignature().getDeclaringTypeName().contains("Map"))
 			 && thisJoinPointStaticPart.getSignature().getName().contains("put"));

	before(Object i): HasNext_mapPut(i){

		mycustommonitors.HasNextRuntimeMonitor.updatedMap(i);

	}

	pointcut HasNext_hasnext(Object i) : call(* hasNext*(..)) && target(i) 
	&& if( (
			thisJoinPointStaticPart.getSignature().getDeclaringTypeName().contains("Iterator"))
 			 && thisJoinPointStaticPart.getSignature().getName().contains("hasNext"));

	before(Object i): HasNext_hasnext(i){

		mycustommonitors.HasNextRuntimeMonitor.hasNextEvent(i);

	}

	pointcut HasNext_N(Object i) : call(*  next*(..)) && target(i) 
	&& if( (
			thisJoinPointStaticPart.getSignature().getDeclaringTypeName().contains("Iterator"))
 			 && thisJoinPointStaticPart.getSignature().getName().contains("next"));

	after(Object i): HasNext_N(i){

		mycustommonitors.HasNextRuntimeMonitor.nextEvent(i);

	}

	pointcut HasNext_createdIterator(Object i) : call(* iterator*(..))  && target(i) 
    && if( (
    		thisJoinPointStaticPart.getSignature().getDeclaringTypeName().contains("Collection"))
 			 && thisJoinPointStaticPart.getSignature().getName().contains("iterator"));

	after(Object i) returning (Object t): HasNext_createdIterator(i){

		mycustommonitors.HasNextRuntimeMonitor.createdIterator(i, t);

	}

	pointcut HasNext_createdIteratorList(Object i) : call(*  iterator*(..)) && target(i) 
	 && if( (
			 thisJoinPointStaticPart.getSignature().getDeclaringTypeName().contains("List"))
	 			 && thisJoinPointStaticPart.getSignature().getName().contains("iterator"));

	after(Object i) returning (Object t): HasNext_createdIteratorList(i){

		mycustommonitors.HasNextRuntimeMonitor.createdIterator(i, t);

	}

}
