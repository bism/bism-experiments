import claps.bism.transformers.StaticInvocation;
import claps.bism.transformers.Transformer;
import claps.bism.transformers.dynamiccontext.DynamicValue;
import claps.bism.transformers.dynamiccontext.MethodCallDynamicContext;

import claps.bism.transformers.staticcontext.MethodCall;



public class HasNextTransformer extends Transformer {


    @Override
    public void beforeMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

        if (methodCall.methodName.contains("hasNext") && methodCall.methodOwner.contains("Iterator")) {

            DynamicValue it = dc.getMethodReceiver(methodCall);

            StaticInvocation sti =
                    new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "hasNextEvent");
            sti.addParameter(it);
            invoke(sti);


        }

        if (methodCall.methodName.contains("add") && methodCall.methodOwner.contains("List")) {

            DynamicValue it = dc.getMethodReceiver(methodCall);


            StaticInvocation sti =
                    new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "updatedList");
            sti.addParameter(it);
            invoke(sti);


        }
        if (methodCall.methodName.contains("put") && methodCall.methodOwner.contains("Map")) {


            DynamicValue it = dc.getMethodReceiver(methodCall);


            claps.bism.transformers.StaticInvocation sti =
                    new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "updatedMap");
            sti.addParameter(it);
            invoke(sti);



        }

    }

    @Override
    public void afterMethodCall(MethodCall methodCall, MethodCallDynamicContext dc) {

        if (methodCall.methodName.contains("next") && methodCall.methodOwner.contains("Iterator")) {
            DynamicValue it = dc.getMethodReceiver(methodCall);

            StaticInvocation sti =
                    new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "nextEvent");
            sti.addParameter(it);
            invoke(sti);


        }
        if (methodCall.methodName.contains("iterator") && methodCall.methodOwner.contains("List")) {

            DynamicValue it = dc.getMethodReceiver(methodCall);
            DynamicValue r = dc.getMethodResult(methodCall);


            StaticInvocation sti =
                    new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "createdIterator");
            sti.addParameter(r);
            sti.addParameter(it);
            invoke(sti);

        }

        if (methodCall.methodName.contains("iterator") && methodCall.methodOwner.contains("Collection")) {

            DynamicValue it = dc.getMethodReceiver(methodCall);
            DynamicValue r = dc.getMethodResult(methodCall);

            StaticInvocation sti =
                    new StaticInvocation("mycustommonitors/HasNextRuntimeMonitor", "createdIterator");
            sti.addParameter(r);
            sti.addParameter(it);
            invoke(sti);


        }


    }

}
