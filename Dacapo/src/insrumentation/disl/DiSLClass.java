import ch.usi.dag.disl.annotation.After ;
import ch.usi.dag.disl.annotation.Before ;
import ch.usi.dag.disl.dynamiccontext.DynamicContext;
import ch.usi.dag.disl.marker.BodyMarker;
import ch.usi.dag.disl.processorcontext.ArgumentProcessorContext;
import ch.usi.dag.disl.processorcontext.ArgumentProcessorMode;

import java.util.Iterator;


public class DiSLClass {
    

   //Replace the scope with the below to build
   
   //org.apache.fop.*.*(..)   for fop
   //org.apache.batik.*.*(..)	for batik
   //avrora.*.*(..)		for avrora
   //org.sunflow.*.*(..)	for sunflow
   //net.*.*(..)		for pmd
   //org.h2.*.*(..)		for h2
   //org.apache.xalan.*.*(..)	for xalan
    

    
    @Before(marker = HasNextInvocationMarker.class  ,
    guard=CustomGuard.class,
    scope="org.apache.fop.*.*(..)")
    public static void beforeHasNext(ArgumentProcessorContext pc) {
        
        Object i =   pc.getReceiver(ArgumentProcessorMode.CALLSITE_ARGS);
        mycustommonitors.HasNextRuntimeMonitor.hasNextEvent(i);
  
        
    }
    

@After(marker = NextInvocationMarker.class ,
guard=CustomGuard.class,
scope="org.apache.fop.*.*(..)")
public static void afterNext(ArgumentProcessorContext pc) {
    

         Object i =   pc.getReceiver(ArgumentProcessorMode.CALLSITE_ARGS);
         mycustommonitors.HasNextRuntimeMonitor.nextEvent(i);

    
    
}

	//List create and update
	
	   @Before(marker = CreateIteratorMarker.class  ,
	   guard=CustomGuard.class,
	   scope="org.apache.fop.*.*(..)")
	    public static void beforeCreateIterator(ArgumentProcessorContext pc, DynamicContext dc) {
	        
	        Object i =   pc.getReceiver(ArgumentProcessorMode.CALLSITE_ARGS);
	        Object s =   dc.getStackValue (0, java.util.List.class);
	       	        
	        mycustommonitors.HasNextRuntimeMonitor.createdIterator(i,s);
	        
	        
	    }
	   
       @Before(marker = UpdateListMarker.class  ,
       guard=CustomGuard.class,
       scope="org.apache.fop.*.*(..)")
       public static void updateList(ArgumentProcessorContext pc) {
           
           Object i =   pc.getReceiver(ArgumentProcessorMode.CALLSITE_ARGS);      
           mycustommonitors.HasNextRuntimeMonitor.updatedList(i);
           
           
       }
       
       
      // Collection Create and update
	   
	   
	   @Before(marker = CreatedCollectionIteratorMarker.class  ,
	   guard=CustomGuard.class,
	   scope="org.apache.fop.*.*(..)")
       public static void beforeCreateCollectionIterator(ArgumentProcessorContext pc, DynamicContext dc) {
           
           Object i =   pc.getReceiver(ArgumentProcessorMode.CALLSITE_ARGS);
           Object s =   dc.getStackValue (0, java.util.Collection.class);
                   
           mycustommonitors.HasNextRuntimeMonitor.createdIterator(i,s);
           
           
       }
	   
	   
       @Before(marker = UpdateMapMarker.class  ,
       guard=CustomGuard.class,
       scope="org.apache.fop.*.*(..)")
       public static void updateMap(ArgumentProcessorContext pc, DynamicContext dc) {
           
           Object i =   pc.getReceiver(ArgumentProcessorMode.CALLSITE_ARGS);
       
                   
           mycustommonitors.HasNextRuntimeMonitor.updatedMap(i);
           
           
       }
	   
	  


	
 
}
