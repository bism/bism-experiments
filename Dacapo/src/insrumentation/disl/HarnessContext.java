import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.InsnList;

import ch.usi.dag.disl.staticcontext.AbstractStaticContext;

public class HarnessContext extends AbstractStaticContext {

	public boolean isHarness() {
	 
		return (staticContextData.getClassNode().name.contains ("harness"));
	}

}
