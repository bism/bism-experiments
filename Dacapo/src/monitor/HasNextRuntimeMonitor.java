package mycustommonitors;

import java.text.AttributedCharacterIterator;
import java.util.*;


public class HasNextRuntimeMonitor {

    public static int eventsCounter = 0;
    public static int nullCounter = 0;


    public static HashSet<Integer> typesEmitted = new HashSet<>();

    public static void saveType(Object b) {


        typesEmitted.add(System.identityHashCode(b));
        nullCounter = typesEmitted.size();

    }


    public static void incrementCounter() {

        eventsCounter++;

    }

    public static void count(int x) {
        incrementCounter();
    }

    public static void hasNextEvent(Object i) {

        incrementCounter();
        saveType(i);

    }

    public static void nextEvent(Object i) {

        incrementCounter();
        saveType(i);

    }

    public static void hasNextEvent(int i) {

        incrementCounter();
        saveType(i);

    }

    public static void createdIterator(Object i) {

        incrementCounter();
        saveType(i);

    }


    public static void createdIterator(AttributedCharacterIterator i, Object s) {

        incrementCounter();
        saveType(i);
        saveType(s);

    }

    public static void createdIterator(Object i, Object s) {

        incrementCounter();
        saveType(s);

    }

    public static void createdIterator(ListIterator i, Object s) {

        incrementCounter();
        saveType(s);

    }


    public static void createdIterator(Iterator i, Object s) {

        incrementCounter();
        saveType(s);
    }

    public static void updatedList(Object i) {

        incrementCounter();
        saveType(i);

    }

    public static void updatedMap(Object i) {

        incrementCounter();
        saveType(i);

    }

    public static void updatedMap(int i) {

        incrementCounter();
        saveType(i);

    }

    public static void nextEvent(int i) {

        incrementCounter();
        saveType(i);

    }


}
