# Financial Transactions  

We compare BISM with DiSL and AspectJ in a runtime verification scenario to monitor some properties of a financial transaction system. 

We use the implementation from the First International Competition on Runtime Verification [CRV-14](https://gitlab.inria.fr/crv14/benchmarks/) to monitor the following properties: 

* Property P1: only users based in certain countries can be Silver or Gold users. 
* Property P2: the transaction system must be initialized before any user logs in. 
*Property P3: no account may end up with a negative balance after being accessed. 
 * Property P4: an account approved by the administrator may not have the same account number as any other already existing account in the system. 
 * Property P5: once a user is disabled by the administrator, he or she may not withdraw from an account until being activated again by the administrator. 
 * Property P6: once greylisted, a user must perform at least three deposits from external before being whitelisted. 
 * Property P7: no user may request more than 10 new accounts in a single session.
 * Property P8: the administrator must reconcile accounts every 1000 external transfers or an aggregate total of one million dollars in external transfers.
 * Property P9: a user may not have more than three active sessions at once.
 * Property P10: transfers may only be made during an active session (i.e., between a login and logout). 
 
 
# How to Run

The experiment can be run in load-time and build-time instrumentation.

* [buildtime](buildtime): to run in build-time instrumentation mode.
* [loadtime](loadtime): to run in loadtime-time instrumentation mode.
* [plot](plot): to generate the plots after running the experiments