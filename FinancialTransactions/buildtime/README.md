# Executing the Experiment

To execute, simply run:

```
make
```


To clean all generated file:

```
make clean
```
