
toolarray=(DiSL)
N=100
tsleep=5s
msleep=3s
pause=2s

#========== RunTime ==========
echo "runtime ..."
for tool in "${toolarray[@]}"
do
	
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		  for scenario in 1 2 3 4 5 6 7 8 9 10
		do
	    output="$(../bin/disl.py -d ../output -s_nodynamicbypass -s_noexcepthandler -- build/inst.jar -noverify -jar time.jar $scenario 2>&1)"
	    echo "P$scenario,$tool,$output" >> time.csv
	    sleep $tsleep
		done
	
	done 
done
sleep $pause
#
#===== Memory Footprint =====
echo "memory footprint ..."
for tool in "${toolarray[@]}"
do
	
	#
    for ((i=0; i<N; i++))
	do
		output=$""
		 for scenario in 1 2 3 4 5 6 7 8 9 10
		do
	     output="$(../bin/disl.py -d ../output -s_nodynamicbypass -s_noexcepthandler -- build/inst.jar -noverify -jar memory.jar $scenario 2>&1)"
	     echo "P$scenario,$tool,$output" >> memory.csv
	    sleep $msleep
		done
	done 
done

 