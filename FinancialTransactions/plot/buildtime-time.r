library(ggplot2)
library(dplyr)

df <- read.csv("buildtime-time.csv", dec=".")

df$tool <- factor(df$tool, levels=c("Original", "DiSL", "AspectJ", "BISM"))
df$benchmark <- factor(df$benchmark, levels=c("P1", "P2", "P3", "P4", "P5", "P6", "P7", "P8", "P9","P10"))

df_fixed <- df %>% 
  group_by(benchmark, tool) %>%
  summarise(
    n=n(),
    mean=mean(execution),
    sd=sd(execution)
  ) %>%
  mutate(se=sd/sqrt(n))


  ggplot(df_fixed) +
  geom_bar(aes(x=benchmark,y=mean, fill=tool), stat="identity", width=.5, position = position_dodge(0.6)) +
  geom_errorbar(aes(x=benchmark, fill=tool, ymin=mean-se, ymax=mean+se), width=.35, position = position_dodge(0.6), alpha=0.7) +
  scale_fill_manual(values = c( "#FDCC8a", "#fc8d59","#d7301f","#7f0000")) +
  theme_minimal() +
  ylab(element_blank()) +
  xlab(element_blank()) +
  guides(fill=guide_legend(title=element_blank())) +
  theme(legend.position="top" ,panel.grid.major.x = element_blank(),text = element_text(size=20)) +
  ggsave("buildtime-time.pdf")   


