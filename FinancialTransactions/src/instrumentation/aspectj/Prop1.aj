import transaction.*;

public aspect Prop1 {
    before(Integer uid) : call(* Interface.ADMIN_makeGoldUser(Integer)) && args(uid){
        MonitorProp1.userType(uid, 1);
    }
    before(Integer uid) : call(* Interface.ADMIN_makeSilverUser(Integer)) && args(uid){
        MonitorProp1.userType(uid, 2);
    }
    before(Integer uid) : call(* Interface.ADMIN_makeNormalUser(Integer)) && args(uid){
        MonitorProp1.userType(uid, 3);
    }
}