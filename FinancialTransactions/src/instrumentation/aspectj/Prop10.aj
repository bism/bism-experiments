import transaction.*;

public aspect Prop10 {
    before(Integer uid) :
            call(* Interface.USER_login(Integer)) && args(uid){
        MonitorProp10.login(uid);
    }
    before(Integer uid) :
            call(* Interface.USER_logout(Integer,*)) && args(uid,*){
        MonitorProp10.logout(uid);
    }
    before(Integer uid) :
            call(* Interface.USER_payToExternal(Integer,..)) && args(uid,..){
        MonitorProp10.payToExternal(uid);
    }
}