import transaction.*;

public aspect Prop2 {
    before(): call(* Interface.ADMIN_initialise(..)){
        MonitorProp2.initialise();
    }

    before(): call(* Interface.USER_login(..)){
        MonitorProp2.login();
    }
}