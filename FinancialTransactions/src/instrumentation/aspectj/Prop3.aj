import transaction.*;

public aspect Prop3 {
    before(Integer uid, String from) :
            call(* Interface.USER_payToExternal(Integer, *, String, *)) && args(uid,*,from,*){
        MonitorProp3.payToExternal(uid, from);
    }

    before(Integer from_uid, String from_account) :
            call(* Interface.USER_transferToOtherAccount(Integer, *, String, ..)) && args(from_uid, *,from_account,..){
        MonitorProp3.transferToOtherAccount(from_uid, from_account);
    }

    before(Integer uid, String from_account) :
            call(* Interface.USER_transferOwnAccounts(Integer, *, String, ..)) && args(uid, *,from_account,..){
        MonitorProp3.transferOwnAccounts(uid, from_account);
    }
}