import transaction.*;

public aspect Prop4 {
    before(String account_number) :
            call(* Interface.ADMIN_approveOpenAccount(*, String)) && args(*, account_number){
        MonitorProp4.approveOpenAccount(account_number);
    }
}