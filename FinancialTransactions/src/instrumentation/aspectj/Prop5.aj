import transaction.*;

public aspect Prop5 {
    before(Integer uid) :
            call(* Interface.ADMIN_disableUser(Integer)) && args(uid){
        MonitorProp5.disableUser(uid);
    }
    before(Integer uid) :
            call(* Interface.ADMIN_activateUser(Integer)) && args(uid){
        MonitorProp5.activateUser(uid);
    }
    before(Integer uid) :
            call(* Interface.USER_payToExternal(Integer, ..)) && args(uid, ..){
        MonitorProp5.payToExternal(uid);
    }
    before(Integer from_uid) :
            call(* Interface.USER_transferToOtherAccount(Integer, ..)) && args(from_uid,..){
        MonitorProp5.transferToOtherAccount(from_uid);
    }
    before(Integer uid) :
            call(* Interface.USER_transferOwnAccounts(Integer, ..)) && args(uid,..){
        MonitorProp5.transferOwnAccounts(uid);
    }
}