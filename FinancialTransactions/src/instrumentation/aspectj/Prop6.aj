import transaction.*;

public aspect Prop6 {
    before(Integer uid) :
            call(* Interface.ADMIN_greylistUser(Integer)) && args(uid){
        MonitorProp6.greylistUser(uid);
    }
    before(Integer uid) :
            call(* Interface.USER_depositFromExternal(Integer, ..)) && args(uid, ..){
        MonitorProp6.depositFromExternal(uid);
    }
    before(Integer uid) :
            call(* Interface.ADMIN_whitelistUser(Integer)) && args(uid){
        MonitorProp6.whitelistUser(uid);
    }
}