import transaction.*;

public aspect Prop7 {
    after() returning(Integer sid) :
            call(* Interface.USER_login(*)){
        MonitorProp7.login(sid);
    }
    before(Integer sid) :
            call(* Interface.USER_requestAccount(*, Integer)) && args(*, sid){
        MonitorProp7.requestAccount(sid);
    }
    before(Integer sid) :
            call(* transaction.Interface.USER_logout(*, Integer)) && args(*, sid){
        MonitorProp7.logout(sid);
    }
}