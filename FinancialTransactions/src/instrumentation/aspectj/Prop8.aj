import transaction.*;

public aspect Prop8 {
    before(double amount) :
            call(* transaction.Interface.USER_payToExternal(*,*,*,double)) && args(*,*,*,amount){
        MonitorProp8.payToExternal(amount);
    }
    before() :
            call(* transaction.Interface.ADMIN_reconcile(..)){
        MonitorProp8.reconcile();
    }
}