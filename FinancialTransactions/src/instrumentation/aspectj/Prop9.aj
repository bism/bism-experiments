import transaction.*;

public aspect Prop9 {
    before(Integer uid) :
            call(* transaction.Interface.USER_login(Integer)) && args(uid){
        MonitorProp9.login(uid);
    }
    before(Integer uid) :
            call(* transaction.Interface.USER_logout(Integer,*)) && args(uid,*){
        MonitorProp9.logout(uid);
    }
}