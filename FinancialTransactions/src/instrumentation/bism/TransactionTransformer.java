import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransactionTransformer extends Transformer {

    

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("ADMIN_makeGoldUser")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp1", "userType");
			DynamicValue p = dc.getMethodArgs(mc, 1); 
			sti.addParameter(p);
			sti.addParameter(1);
			invoke(sti);
		}
		
		if(mc.methodName.equals("ADMIN_makeSilverUser")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp1", "userType");
			DynamicValue p = dc.getMethodArgs(mc, 1); 
			sti.addParameter(p);
			sti.addParameter(2);
			invoke(sti);
		}
		
		if(mc.methodName.equals("ADMIN_makeNormalUser")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp1", "userType");
			DynamicValue p = dc.getMethodArgs(mc, 1);  
			sti.addParameter(p);
			sti.addParameter(3);			
			invoke(sti);
		}



		if(mc.methodName.equals("ADMIN_initialise")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp2", "initialise");
			invoke(sti);
		}
		
		if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp2", "login");
			invoke(sti);
		}


        if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation(transaction/MonitorProp3, "payToExternal");
			
			DynamicValue p1 = dc.getMethodArgs(mc,1);  
			DynamicValue p2 = dc.getMethodArgs(mc, 3);  
			sti.addParameter(p1);
			sti.addParameter(p2);
			invoke(sti);
		}
		
		if(mc.methodName.equals("USER_transferToOtherAccount")){
			StaticInvocation sti = new StaticInvocation(transaction/MonitorProp3, "transferToOtherAccount");

			DynamicValue p1 = dc.getMethodArgs(mc, 1);  
			DynamicValue p2 = dc.getMethodArgs(mc, 3);  
			sti.addParameter(p1);
			sti.addParameter(p2);			
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_transferOwnAccounts")){
			StaticInvocation sti = new StaticInvocation(transaction/MonitorProp3, "transferOwnAccounts");

			DynamicValue p1 = dc.getMethodArgs(mc, 1);  
			DynamicValue p2 = dc.getMethodArgs(mc, 3);  
			sti.addParameter(p1);
			sti.addParameter(p2);			
			invoke(sti);
		}


        if(mc.methodName.equals("ADMIN_approveOpenAccount")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp4", "approveOpenAccount");
			
			DynamicValue p = dc.getMethodArgs(mc,2); 
			sti.addParameter(p);
			invoke(sti);
		}


        if(mc.methodName.equals("ADMIN_disableUser")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp5", "disableUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("ADMIN_activateUser")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp5", "activateUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp5", "payToExternal");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_transferToOtherAccount")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp5", "transferToOtherAccount");

			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_transferOwnAccounts")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp5", "transferOwnAccounts");

			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}


        if(mc.methodName.equals("ADMIN_greylistUser")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp6", "greylistUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_depositFromExternal")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp6", "depositFromExternal");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("ADMIN_whitelistUser")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp6", "whitelistUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}


        if(mc.methodName.equals("USER_requestAccount")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp7", "requestAccount");
			DynamicValue p = dc.getMethodArgs(mc,2); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_logout")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp7", "logout");
			DynamicValue p = dc.getMethodArgs(mc,2); 
			sti.addParameter(p);
			invoke(sti);
		}

        if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp8", "payToExternal");
			DynamicValue p = dc.getMethodArgs(mc,4); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("ADMIN_reconcile")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp8", "reconcile");
			invoke(sti);
		}

        if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp9", "login");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_logout")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp9", "logout");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);			
			invoke(sti);
		}


        if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp10", "login");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_logout")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp10", "logout");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);			
			invoke(sti);
		}

		if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp10", "payToExternal");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_transferToOtherAccount")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp10", "transferToOtherAccount");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_transferOwnAccounts")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp10", "transferOwnAccounts");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}


    }

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

        if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation("transaction/MonitorProp7", "login");
			DynamicValue p = dc.getMethodResult(mc); 
			sti.addParameter(p);
			invoke(sti);
		}

        
    }

}


