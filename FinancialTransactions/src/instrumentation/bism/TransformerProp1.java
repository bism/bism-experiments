//
//PROPERTY 1: Only users based in certain countries can be Silver or Gold users  
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp1 extends Transformer {

    public String monitor = "transaction/MonitorProp1";

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("ADMIN_makeGoldUser")){
			StaticInvocation sti = new StaticInvocation(monitor, "userType");
			DynamicValue p = dc.getMethodArgs(mc, 1); 
			sti.addParameter(p);
			sti.addParameter(1);
			invoke(sti);
		}
		
		if(mc.methodName.equals("ADMIN_makeSilverUser")){
			StaticInvocation sti = new StaticInvocation(monitor, "userType");
			DynamicValue p = dc.getMethodArgs(mc, 1); 
			sti.addParameter(p);
			sti.addParameter(2);
			invoke(sti);
		}
		
		if(mc.methodName.equals("ADMIN_makeNormalUser")){
			StaticInvocation sti = new StaticInvocation(monitor, "userType");
			DynamicValue p = dc.getMethodArgs(mc, 1);  
			sti.addParameter(p);
			sti.addParameter(3);			
			invoke(sti);
		}

    }
}

