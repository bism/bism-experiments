//
// PROPERTY 10: Transfers may only be made during an active session (i.e. between a login and a logout)
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp10 extends Transformer {

    public String monitor = "transaction/MonitorProp10";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation(monitor, "login");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_logout")){
			StaticInvocation sti = new StaticInvocation(monitor, "logout");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);			
			invoke(sti);
		}

		if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation(monitor, "payToExternal");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_transferToOtherAccount")){
			StaticInvocation sti = new StaticInvocation(monitor, "transferToOtherAccount");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_transferOwnAccounts")){
			StaticInvocation sti = new StaticInvocation(monitor, "transferOwnAccounts");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
    }
}

