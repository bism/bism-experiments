//
//PROPERTY 2: The transaction system must be initialised before any user logs in.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;

public class TransformerProp2 extends Transformer {

    public String monitor = "transaction/MonitorProp2";

    @Override
    public void beforeMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("ADMIN_initialise")){
			StaticInvocation sti = new StaticInvocation(monitor, "initialise");
			invoke(sti);
		}
		
		if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation(monitor, "login");
			invoke(sti);
		}
        
    }
}

