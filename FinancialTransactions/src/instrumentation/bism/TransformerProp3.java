//
// PROPERTY 3: No account may end up with a negative balance after being accessed.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp3 extends Transformer {

    public String monitor = "transaction/MonitorProp3";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation(monitor, "payToExternal");
			
			DynamicValue p1 = dc.getMethodArgs(mc,1);  
			DynamicValue p2 = dc.getMethodArgs(mc, 3);  
			sti.addParameter(p1);
			sti.addParameter(p2);
			invoke(sti);
		}
		
		if(mc.methodName.equals("USER_transferToOtherAccount")){
			StaticInvocation sti = new StaticInvocation(monitor, "transferToOtherAccount");

			DynamicValue p1 = dc.getMethodArgs(mc, 1);  
			DynamicValue p2 = dc.getMethodArgs(mc, 3);  
			sti.addParameter(p1);
			sti.addParameter(p2);			
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_transferOwnAccounts")){
			StaticInvocation sti = new StaticInvocation(monitor, "transferOwnAccounts");

			DynamicValue p1 = dc.getMethodArgs(mc, 1);  
			DynamicValue p2 = dc.getMethodArgs(mc, 3);  
			sti.addParameter(p1);
			sti.addParameter(p2);			
			invoke(sti);
		}

    }
}

