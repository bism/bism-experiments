//
// PROPERTY 4: An account approved by the administrator may not have the same 
		// account number as any other already existing account in the system.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp4 extends Transformer {

    public String monitor = "transaction/MonitorProp4";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("ADMIN_approveOpenAccount")){
			StaticInvocation sti = new StaticInvocation(monitor, "approveOpenAccount");
			
			DynamicValue p = dc.getMethodArgs(mc,2); 
			sti.addParameter(p);
			invoke(sti);
		}
        
    }
}

