//
// PROPERTY 5: Once a user is disabled by the administrator, he or she may not
// withdraw from an account until being activated again by the administrator.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp5 extends Transformer {

    public String monitor = "transaction/MonitorProp5";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("ADMIN_disableUser")){
			StaticInvocation sti = new StaticInvocation(monitor, "disableUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("ADMIN_activateUser")){
			StaticInvocation sti = new StaticInvocation(monitor, "activateUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation(monitor, "payToExternal");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_transferToOtherAccount")){
			StaticInvocation sti = new StaticInvocation(monitor, "transferToOtherAccount");

			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

		if(mc.methodName.equals("USER_transferOwnAccounts")){
			StaticInvocation sti = new StaticInvocation(monitor, "transferOwnAccounts");

			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

    }
}

