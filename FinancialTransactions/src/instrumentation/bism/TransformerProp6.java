//
// PROPERTY 6: Once greylisted, a user must perform at least three deposits from external
// before being whitelisted.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp6 extends Transformer {

    public String monitor = "transaction/MonitorProp6";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("ADMIN_greylistUser")){
			StaticInvocation sti = new StaticInvocation(monitor, "greylistUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_depositFromExternal")){
			StaticInvocation sti = new StaticInvocation(monitor, "depositFromExternal");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("ADMIN_whitelistUser")){
			StaticInvocation sti = new StaticInvocation(monitor, "whitelistUser");
			
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}

    }
}

