//
// PROPERTY 7: No user may request more than 10 new accounts in a single session.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp7 extends Transformer {

    public String monitor = "transaction/MonitorProp7";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation(monitor, "login");
			DynamicValue p = dc.getMethodResult(mc); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_requestAccount")){
			StaticInvocation sti = new StaticInvocation(monitor, "requestAccount");
			DynamicValue p = dc.getMethodArgs(mc,2); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_logout")){
			StaticInvocation sti = new StaticInvocation(monitor, "logout");
			DynamicValue p = dc.getMethodArgs(mc,2); 
			sti.addParameter(p);
			invoke(sti);
		}

    }
}

