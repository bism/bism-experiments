//
// PROPERTY 8: The administrator must reconcile accounts every 1000 external money
// transfers or an aggregate total of one million dollars in external transfers.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp8 extends Transformer {

    public String monitor = "transaction/MonitorProp8";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("USER_payToExternal")){
			StaticInvocation sti = new StaticInvocation(monitor, "payToExternal");
			DynamicValue p = dc.getMethodArgs(mc,4); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("ADMIN_reconcile")){
			StaticInvocation sti = new StaticInvocation(monitor, "reconcile");
			invoke(sti);
		}
    }
}

