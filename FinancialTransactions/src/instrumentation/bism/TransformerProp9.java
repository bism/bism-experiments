//
// PROPERTY 9: A user may not have more than 3 active sessions at once.
//
import inria.bism.transformers.Transformer;
import inria.bism.transformers.dynamiccontext.MethodCallDynamicContext;
import inria.bism.transformers.staticcontext.MethodCall;
import inria.bism.transformers.StaticInvocation;
import inria.bism.transformers.dynamiccontext.DynamicValue;

public class TransformerProp9 extends Transformer {

    public String monitor = "transaction/MonitorProp9";

    @Override
    public void afterMethodCall(MethodCall mc, MethodCallDynamicContext dc) {

		if(mc.methodName.equals("USER_login")){
			StaticInvocation sti = new StaticInvocation(monitor, "login");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);
			invoke(sti);
		}
        
		if(mc.methodName.equals("USER_logout")){
			StaticInvocation sti = new StaticInvocation(monitor, "logout");
			DynamicValue p = dc.getMethodArgs(mc,1); 
			sti.addParameter(p);			
			invoke(sti);
		}
    }
}

