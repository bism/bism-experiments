import ch.usi.dag.disl.annotation.After;
import ch.usi.dag.disl.annotation.AfterReturning;
import ch.usi.dag.disl.annotation.Before;
import ch.usi.dag.disl.processorcontext.ArgumentProcessorContext;
import ch.usi.dag.disl.processorcontext.ArgumentProcessorMode;
import ch.usi.dag.disl.dynamiccontext.DynamicContext;
 

public class DiSLClass {

//Prop1

@Before(marker = MethodInvocationMarker11.class, scope="transaction.*.*")
public static void event11(ArgumentProcessorContext pc, DynamicContext dc) {        
      
        Integer p = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
        transaction.MonitorProp1.userType(p,1);
    
}

@Before(marker = MethodInvocationMarker12.class, scope="transaction.*.*")
public static void event12( ArgumentProcessorContext pc, DynamicContext dc ) {        
    Integer p = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
    transaction.MonitorProp1.userType(p,2);
}


@Before(marker = MethodInvocationMarker13.class, scope="transaction.*.*")
public static void event13( ArgumentProcessorContext pc, DynamicContext dc ) {     
    Integer p = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
    transaction.MonitorProp1.userType(p,3);
}



//Prop2
@Before(marker = MethodInvocationMarker21.class, scope="transaction.*.*")
public static void event21( ) {      
        transaction.MonitorProp2.initialise();
}
@Before(marker = MethodInvocationMarker22.class, scope="transaction.*.*")
public static void event22() {        
        transaction.MonitorProp2.login();
}



//Prop3
@Before(marker = MethodInvocationMarker31.class, scope="transaction.*.*")
public static void event31( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p1 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
    String p3 = (String) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[2];
        transaction.MonitorProp3.payToExternal(p1,p3);
}
@Before(marker = MethodInvocationMarker32.class, scope="transaction.*.*")
public static void event32( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p1 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
    String p3 = (String) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[2];
 
        transaction.MonitorProp3.transferToOtherAccount(p1,p3);
}
@Before(marker = MethodInvocationMarker33.class, scope="transaction.*.*")
public static void event33( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p1 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
    String p3 = (String) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[2];
   transaction.MonitorProp3.transferOwnAccounts(p1,p3);
}




//Prop4
@Before(marker = MethodInvocationMarker41.class, scope="transaction.*.*")
public static void event41( ArgumentProcessorContext pc, DynamicContext dc ) {    
    
   String p3 = (String) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[1];   
   transaction.MonitorProp4.approveOpenAccount(p3);
}




//Prop5

@Before(marker = MethodInvocationMarker51.class, scope="transaction.*.*")
public static void event51( ArgumentProcessorContext pc, DynamicContext dc ) {   
 
   Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
   transaction.MonitorProp5.disableUser(p3);
}

@Before(marker = MethodInvocationMarker52.class, scope="transaction.*.*")
public static void event52( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
   transaction.MonitorProp5.activateUser(p3);
}

@Before(marker = MethodInvocationMarker53.class, scope="transaction.*.*")
public static void event53( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp5.payToExternal(p3);
}

@Before(marker = MethodInvocationMarker54.class, scope="transaction.*.*")
public static void event54( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];  
   transaction.MonitorProp5.transferToOtherAccount(p3);
}

@Before(marker = MethodInvocationMarker55.class, scope="transaction.*.*")
public static void event55( ArgumentProcessorContext pc, DynamicContext dc ) {

   Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0]; 
   transaction.MonitorProp5.transferOwnAccounts(p3);
}

//Prop6

@Before(marker = MethodInvocationMarker61.class, scope="transaction.*.*")
public static void event61( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
   transaction.MonitorProp6.greylistUser(p3);
}

@Before(marker = MethodInvocationMarker62.class, scope="transaction.*.*")
public static void event62( ArgumentProcessorContext pc, DynamicContext dc ) {    
    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];  
   transaction.MonitorProp6.depositFromExternal(p3);
}

@Before(marker = MethodInvocationMarker63.class, scope="transaction.*.*")
public static void event63( ArgumentProcessorContext pc, DynamicContext dc ) {    
  Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];
   transaction.MonitorProp6.whitelistUser(p3);
}




//Prop7


@AfterReturning(marker = MethodInvocationMarker71.class, scope="transaction.*.*")
public static void event71( ArgumentProcessorContext pc, DynamicContext dc ) {    

    Integer p3 =   dc.getStackValue (0, Integer.class); 
    transaction.MonitorProp7.login(p3);
   
}

@Before(marker = MethodInvocationMarker72.class, scope="transaction.*.*")
public static void event72( ArgumentProcessorContext pc, DynamicContext dc ) {    

    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[1];
   transaction.MonitorProp7.requestAccount(p3);
}

@Before(marker = MethodInvocationMarker73.class, scope="transaction.*.*")
public static void event73( ArgumentProcessorContext pc, DynamicContext dc ) {    

    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[1];   
   transaction.MonitorProp7.logout(p3);
}

//Prop8

@Before(marker = MethodInvocationMarker81.class, scope="transaction.*.*")
public static void event81( ArgumentProcessorContext pc, DynamicContext dc ) {    

   double p3 = (double) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[3];
   transaction.MonitorProp8.payToExternal(p3);
}

@Before(marker = MethodInvocationMarker82.class, scope="transaction.*.*")
public static void event82() {    
   transaction.MonitorProp8.reconcile();
}


//Prop9


@Before(marker = MethodInvocationMarker91.class, scope="transaction.*.*")
public static void event91( ArgumentProcessorContext pc, DynamicContext dc ) {    

    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp9.login(p3);
}

@Before(marker = MethodInvocationMarker92.class, scope="transaction.*.*")
public static void event92(ArgumentProcessorContext pc, DynamicContext dc) {    
    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp9.logout(p3);
}

//Prop10

@Before(marker = MethodInvocationMarker101.class, scope="transaction.*.*")
public static void event101( ArgumentProcessorContext pc, DynamicContext dc ) {  
   Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp10.login(p3);
}

@Before(marker = MethodInvocationMarker102.class, scope="transaction.*.*")
public static void event102(ArgumentProcessorContext pc, DynamicContext dc) {    
    Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp10.logout(p3);
}
@Before(marker = MethodInvocationMarker103.class, scope="transaction.*.*")
public static void event103( ArgumentProcessorContext pc, DynamicContext dc ) {    

   Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp10.payToExternal(p3);
}

@Before(marker = MethodInvocationMarker104.class, scope="transaction.*.*")
public static void event104(ArgumentProcessorContext pc, DynamicContext dc) {    
   Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp10.transferToOtherAccount(p3);
}

@Before(marker = MethodInvocationMarker105.class, scope="transaction.*.*")
public static void event105( ArgumentProcessorContext pc, DynamicContext dc ) {    

   Integer p3 = (Integer) pc.getArgs (ArgumentProcessorMode.CALLSITE_ARGS)[0];   
   transaction.MonitorProp10.transferOwnAccounts(p3);
}

 

}