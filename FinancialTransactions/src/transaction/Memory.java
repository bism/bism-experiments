package  transaction;

import java.util.LinkedList; 
import java.util.Queue; 
import java.lang.management.ManagementFactory;
import java.lang.management.GarbageCollectorMXBean;

public class Memory {

	public static long getCurrentlyUsedMemory() {
		return ManagementFactory.getMemoryMXBean().getHeapMemoryUsage().getUsed() +
		ManagementFactory.getMemoryMXBean().getNonHeapMemoryUsage().getUsed();
	}


	public static long getGcCount() {
		long sum = 0;
		for (GarbageCollectorMXBean b : ManagementFactory.getGarbageCollectorMXBeans()) {
			long count = b.getCollectionCount();
			if (count != -1) { 
				sum +=  count; 
			}
		}
		return sum;
	}


	public static long getReallyUsedMemory() {
		long before = getGcCount();
		System.gc();
		while (getGcCount() == before);
		return getCurrentlyUsedMemory();
	}


	public static long getSettledUsedMemory() {
		long m;
		long m2 = getReallyUsedMemory();
		do {
			try{
				Thread.sleep(567);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			} 
			m = m2;
			m2 = getReallyUsedMemory();
		} while (m2 < m);
		return m;
	}


	public static void main(String[] args){
		if(args.length > 0){
			int scenario = Integer.parseInt(args[0]);
			switch(scenario){
				case 1: Scenario1.run(); break;
				case 2: Scenario2.run(); break;
				case 3: Scenario3.run(); break;
				case 4: Scenario4.run(); break;	
				case 5: Scenario5.run(); break;	
				case 6: Scenario6.run(); break;	
				case 7: Scenario7.run(); break;	
				case 8: Scenario8.run(); break;	
				case 9: Scenario9.run(); break;	
				case 10: Scenario10.run(); break;	
				default: System.out.println("Requested scenario is not defined.");
			}
		}
		long memory = getSettledUsedMemory(); 
		System.out.println(memory);
	}

}