package  transaction;

import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class Scenario4 {

    public static void run(){
		String filename = "userinfo_10K.txt";
    	Interface tr = new Interface();
    	
        Integer uid; 
        String name;
        String country;

		UserInfo u;		
		Integer sid;

		String account_number;

        // Initialise
    	tr.ADMIN_initialise();

		//Add Users
    	String line;
        String[] strArr;
        try{
			File file = new File(filename);     
	    	FileReader reader = new FileReader(file);    
	    	BufferedReader buffer = new BufferedReader(reader); 

	    	int counter = 1;

	    	while((line = buffer.readLine())!=null) {
	    		strArr = line.split(" ", 2);
	    		name = strArr[0];
	    		country = strArr[1];

	    		uid = tr.ADMIN_createUser(name, country);

	        	tr.ADMIN_activateUser(uid);

	        	if(counter%2 == 0 && (country.equals("France") || country.equals("Swaziland") || country.equals("United Kingdom") 
	        		|| country.equals("United States") || country.equals("Germany") || country.equals("Japan"))
	        		|| country.equals("Spain") || country.equals("Italy") || country.equals("Greece")){

	        		tr.ADMIN_makeGoldUser(uid);
	        	}
	        	else if(counter%2 != 0 && (country.equals("Luxembourg") || country.equals("Norway")  
	        		|| country.equals("Portugal") || country.equals("Denmark") || country.equals("Belgium")
	        		|| country.equals("Netherlands") || country.equals("Iceland") || country.equals("Australia"))){

	        		tr.ADMIN_makeSilverUser(uid);
	        	}
	        	else{
	        		tr.ADMIN_makeNormalUser(uid);
	        	}

				counter++;
	    	}//end while

	    	reader.close(); 
    	}  
    	catch(IOException e) {  
    		e.printStackTrace();  
    	}

    	//For every user
    	for(int i = 1; i < tr.ts.users.size(); i++){
    		u = tr.ts.users.get(i); //.getUserInfo();
    		uid = u.uid;

    		//login
    		sid = tr.USER_login(uid);

    		//create accounts
    		if(u.isGoldUser()){
    			for (Integer j=0; j<10; j++) {
					account_number = tr.USER_requestAccount(uid,sid);
					tr.ADMIN_approveOpenAccount(uid, account_number);
				}
			}
			else if(u.isSilverUser()){
    			for (Integer j=0; j<5; j++) {
					account_number = tr.USER_requestAccount(uid,sid);
					tr.ADMIN_approveOpenAccount(uid, account_number);
				}
			}
			else{
    			for (Integer j=0; j<3; j++) {
					account_number = tr.USER_requestAccount(uid,sid);
					tr.ADMIN_approveOpenAccount(uid, account_number);
				}
			}

			//logout
			tr.USER_logout(uid, sid);
		}
    }

}