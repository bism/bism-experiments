package  transaction;

import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

public class Scenario6 {

    public static String openAccount(Interface tr, Integer uid, Integer sid, double amount){
		String account_number;
		account_number = tr.USER_requestAccount(uid,sid);
		tr.ADMIN_approveOpenAccount(uid, account_number);
		tr.USER_depositFromExternal(uid, sid, account_number, amount);
		return account_number;
    }

    public static void run(){
		String filename = "userinfo_10K.txt";
    	Interface tr = new Interface();
    	
        Integer uid; 
        String name;
        String country;

		UserInfo u;		
		Integer sid;

		String account_number_1;
		String account_number_2;
		String account_number_3;

        // Initialise
    	tr.ADMIN_initialise();

		//Add Users
    	String line;
        String[] strArr;
        try{
			File file = new File(filename);     
	    	FileReader reader = new FileReader(file);    
	    	BufferedReader buffer = new BufferedReader(reader); 

	    	int counter = 1;

	    	while((line = buffer.readLine())!=null) {
	    		strArr = line.split(" ", 2);
	    		name = strArr[0];
	    		country = strArr[1];

	    		uid = tr.ADMIN_createUser(name, country);

	        	tr.ADMIN_activateUser(uid);

	        	if(counter%2 == 0 && (country.equals("France") || country.equals("Swaziland") || country.equals("United Kingdom") 
	        		|| country.equals("United States") || country.equals("Germany") || country.equals("Japan"))
	        		|| country.equals("Spain") || country.equals("Italy") || country.equals("Greece")){

	        		tr.ADMIN_makeGoldUser(uid);
	        	}
	        	else if(counter%2 != 0 && (country.equals("Luxembourg") || country.equals("Norway")  
	        		|| country.equals("Portugal") || country.equals("Denmark") || country.equals("Belgium")
	        		|| country.equals("Netherlands") || country.equals("Iceland") || country.equals("Australia"))){

	        		tr.ADMIN_makeSilverUser(uid);
	        	}
	        	else{
	        		tr.ADMIN_makeNormalUser(uid);
	        	}

				counter++;
	    	}//end while

	    	reader.close(); 
    	}  
    	catch(IOException e) {  
    		e.printStackTrace();  
    	}

    	//For every user
    	for(int i = 1; i < tr.ts.users.size(); i++){
    		u = tr.ts.users.get(i); //.getUserInfo();
    		uid = u.uid;

    		//login
    		// sid = tr.USER_login(uid);

    		//create accounts
    		if(u.isGoldUser()){
    			sid = tr.USER_login(uid);
    			account_number_1 = openAccount(tr, uid, sid, 2000);
    			account_number_2 = openAccount(tr, uid, sid, 1000);
    			account_number_3 = openAccount(tr, uid, sid, 500);
				tr.USER_logout(uid, sid);

				tr.ADMIN_greylistUser(uid);

				sid = tr.USER_login(uid);
				for (Integer j=0; j<10; j++) {
					tr.USER_depositFromExternal(uid, sid, account_number_1, 100);
				}
				tr.USER_logout(uid, sid);

				tr.ADMIN_whitelistUser(uid);
				tr.ADMIN_greylistUser(uid);

				sid = tr.USER_login(uid);
				for (Integer j=0; j<5; j++) {
					tr.USER_depositFromExternal(uid, sid, account_number_1, 100);
				}
				tr.USER_depositFromExternal(uid, sid, account_number_2, 500);
				tr.USER_logout(uid, sid);

				tr.ADMIN_whitelistUser(uid);
				tr.ADMIN_greylistUser(uid);

				sid = tr.USER_login(uid);
				tr.USER_depositFromExternal(uid, sid, account_number_1, 100);
				tr.USER_depositFromExternal(uid, sid, account_number_2, 100);
				tr.USER_depositFromExternal(uid, sid, account_number_3, 100);
				tr.USER_logout(uid, sid);

				tr.ADMIN_whitelistUser(uid);
			}
			else if(u.isSilverUser()){
    			sid = tr.USER_login(uid);
    			account_number_1 = openAccount(tr, uid, sid, 1500);
    			account_number_2 = openAccount(tr, uid, sid, 1000);
				tr.USER_logout(uid, sid);

				tr.ADMIN_greylistUser(uid);

				sid = tr.USER_login(uid);
				for (Integer j=0; j<5; j++) {
					tr.USER_depositFromExternal(uid, sid, account_number_1, 50);
				}
				tr.USER_logout(uid, sid);

				tr.ADMIN_whitelistUser(uid);
				tr.ADMIN_greylistUser(uid);

				sid = tr.USER_login(uid);
				tr.USER_depositFromExternal(uid, sid, account_number_1, 50);
				tr.USER_depositFromExternal(uid, sid, account_number_1, 50);
				tr.USER_depositFromExternal(uid, sid, account_number_2, 50);
				tr.USER_logout(uid, sid);

				tr.ADMIN_whitelistUser(uid);
			}
			else{
    			sid = tr.USER_login(uid);
    			account_number_1 = openAccount(tr, uid, sid, 500);
				tr.USER_logout(uid, sid);
				
				tr.ADMIN_greylistUser(uid);

				sid = tr.USER_login(uid);
				for (Integer j=0; j<3; j++) {
					tr.USER_depositFromExternal(uid, sid, account_number_1, 25);
				}
				tr.USER_logout(uid, sid);

				tr.ADMIN_whitelistUser(uid);
			}

			//logout
			// tr.USER_logout(uid, sid);
		}
    }

}