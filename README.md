# BISM Experiments

[BISM](https://gitlab.inria.fr/bism/bism-public) is a lightweight [ASM](https://asm.ow2.io/) instrumentation framework designed to instrument Java programs at bytecode level by writing Java syntax.
The framework adopts an aspect-oriented programming paradigm and provides a rich API to facilitate instrumentation that includes: join points, dynamic and static context, and instrumentation methods. 
A program is instrumented statically with minimal overhead and will run without any dependency to BISM. The framework also provides CFG visualization that helps
developers in the instrumentation process.


This repository hosts the experiments we performed to benchmark BISM. 

The results of the experiments can be found in BISM [published papers](https://gitlab.inria.fr/bism/bism-public/-/tree/master/papers)

The table below summarizes the experiments.
 
|              |     |                         |                 |                     |          |
|:------------:|:---:|:-----------------------:|:---------------:|:-------------------:|:--------:|
|  **Experiment**            | **Instrumentation Mode**    | **Performance Metrics** |                 | **Comparison with** |          |
|              |     |       **Runtime**       | **Used Memory** |     **AspectJ**     | **DiSL** |
|     AES      | Load-time  |        Y                 |   Y  (-DiSL)     |        NA             |   Y       |
|              | Build-time  |         Y                |        Y         |        NA          |       Y   |
| Transactions | Load-time  |          Y               |   Y  (-DiSL)     |        Y             |      Y    |
|              | Build-time |           Y              |        Y         |        Y             |       Y   |
|    DaCapo    | Load-time  |            Y             |   Y  (-DiSL)         |        Y             |        Y  |
|              | Build-time  |             Y            |        Y         |        Y             |         Y |



# To Run

To run the experiments, download the repository and navigate to each experiment and read the readme file.

## Download the repository

```
git clone https://gitlab.inria.fr/bism/bism-experiments.git
```

## Exploring the Artifact

Inside each experiment directory, there is README.md and 4 folders:


* buildtime: contains the experiment in buid-time instrumentation mode.
* loadtime: contains the experiment in load-time instrumentation mode.
* src: contains the source files for the base program and instrumentation.
* plot: contains scripts used to generate the plots. 


 

 
